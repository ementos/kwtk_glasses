#define BOOST_TEST_MODULE EigenFacesTest
#include <boost/test/unit_test.hpp>

#include "../src/TrainSetPreparer.h"
#include "../src/EigenFaces.h"

BOOST_AUTO_TEST_CASE(ClassificationTest)
{
    TrainSetPreparer preparer;
    preparer.Prepare("res/kodano/", "res/trained/");
    EigenFaces eigenFaces;
    eigenFaces.Train(preparer.storedTrainFiles);

    for (int i = 0; i < 10; ++i) {
        string fileName = "res/trained/glass_"+std::to_string(i)+".jpg";
        Mat image = imread(fileName, 1 );
        if (image.empty())
        {
            BOOST_TEST_MESSAGE("Missing file " + fileName);
            continue;
        }

        Mat gray_image;
        cvtColor( image, gray_image, CV_BGR2GRAY );
        int detectedClass = eigenFaces.Predict(gray_image);
        BOOST_CHECK_EQUAL(detectedClass, i);
    }
}

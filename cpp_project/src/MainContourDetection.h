#ifndef MAIN_CONTOUR_DETECTION_H
#define MAIN_CONTOUR_DETECTION_H

#include <cv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>

using namespace cv;
using namespace std;

class GlassesPoints {
public:
    bool filled = false;
    Point glass1;
    double radius1;
    Point glass2;
    double radius2;

    Point GetMiddlePoint()
    {
        return Point((glass1.x + glass2.x)/2, (glass1.y + glass2.y)/2);
    }
};

class MainContourDetection {
public:
    MainContourDetection ();
    virtual ~MainContourDetection () {};

    vector<Point> Detect(Mat grayImage);


    vector<Point> GetContourAssociatedWithGlasses(Mat grayImage, GlassesPoints points);


    vector<Point> defectsHull(vector<int> points, vector<Point> contour);


    bool inContour(GlassesPoints points, vector<Point> c);


    GlassesPoints FindGlasses(Mat grayImage);


    static bool Between(double value, double threshold1, double threshold2);
    GlassesPoints glassesPoints;
private:
    Mat resultImg;
};

#endif


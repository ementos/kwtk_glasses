#ifndef EIGENFACES_H
#define EIGENFACES_H

#include "Algorithm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <jni.h>
#include <cv.h>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "MainContourDetection.h"
#include "TrainSetPreparer.h"
#include "jni_md.h"
using namespace cv;

class EigenFaces : MyAlgorithm
{
public:
    EigenFaces(std::vector<std::string> trainingFilesStrings);
    virtual ~EigenFaces();

    void Train();
    bool TrainWithout(std::vector<int> without);
    int Predict(Mat image);
    std::vector<std::string> Predict(std::string filePath);

private:
    Ptr<FaceRecognizer> model;
    TrainSetPreparer preparer;
    vector<Mat> images_train;
    vector<int> labels_train;
    EigenFaces();
};

#ifdef __cplusplus
extern "C" {
#endif

    JNIEXPORT jintArray JNICALL Java_main_EigenfacesConnector_svmPredict (JNIEnv * env, jobject jobj, jstring path, jobjectArray trainingSet);
    JNIEXPORT void JNICALL Java_main_EigenfacesConnector_svmNormalize (JNIEnv * env, jobject jobj, jstring binaryPath, jstring imagePath, jstring normalizedPath);
    //void SVMPredict (const char*);

#ifdef __cplusplus
}
#endif

#endif

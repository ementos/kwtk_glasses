#include "EigenFacesProcess.h"

EigenFacesProcess::EigenFacesProcess(){
    // Prepare train images
    //preparer.Prepare("res/kodano/", "res/trained/");
    // FIXME probably wrong elgeinFeces.preparer is more proper
    //eigenFaces.Train();
}

EigenFacesProcess::~EigenFacesProcess(){}

void EigenFacesProcess::RunProcess(Mat imageToDetect)
{
    Mat image(imageToDetect);
    if (imageToDetect.channels() > 1) {
        cvtColor(imageToDetect, image, CV_BGR2GRAY );
    }

    // Resolution loose is necessary here
    if (image.size().width > 500)
    {
        cv::Size size;
        size.width = 400;
        size.height = (400/(double)image.size().height)*image.size().width;
        resize(image, image, size);
    }

    vector<Point> mainContour = contourDetectionProcess.RunProcess(image);

    GlassNormalizer normalizer;
    Mat normalized = normalizer.Normalize(mainContour, image);

    imshow("Normalized image", normalized);
    waitKey(-1);

    int predictedLabel = 0;// eigenFaces.Predict(normalized);
    std::cout << "Predicted Label for given image = " << predictedLabel << std::endl;
    for (auto imageLabelPair : preparer.storedTrainFiles) {
        if (imageLabelPair.second == predictedLabel) {
            std::cout << "Which means image " << imageLabelPair.first << std::endl;

            imshow("Found image in train set", imread(imageLabelPair.first, 1));
            waitKey(-1);
            break;
        }
    }
}

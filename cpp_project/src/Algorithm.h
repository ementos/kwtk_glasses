#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <cv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

class MyAlgorithm
{
public:
    MyAlgorithm ();
    virtual ~MyAlgorithm ();

protected:
};

#endif

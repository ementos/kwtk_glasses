#include "MainContourDetection.h"

MainContourDetection::MainContourDetection()
{
}

vector<Point> MainContourDetection::Detect(Mat grayImage)
{
    Mat returnImage = grayImage.clone();
    this->resultImg = returnImage;

    Mat workingImage = grayImage.clone();

    glassesPoints = FindGlasses(workingImage);
    if (!glassesPoints.filled)
    {
        std::cout << "Couldn't find circles"<<std::endl;
        return vector<Point>();
    }

    workingImage = grayImage.clone();

    return GetContourAssociatedWithGlasses(workingImage, glassesPoints);
}

vector<Point> MainContourDetection::GetContourAssociatedWithGlasses(Mat grayImage, GlassesPoints points)
{
    vector<vector<Point>> contours;
    Mat binaryImage;
    blur( grayImage, binaryImage, Size(3,3) );
    Canny(binaryImage, binaryImage, 100, 3*100, 3, true);
    imwrite("afterCanny.jpg", binaryImage);
    vector<Vec4i> hierarchy;
    findContours(binaryImage, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
    for(int i=0; i < contours.size();i++) {
        /*
        Mat imcpy = binaryImage.clone();
        for (auto &point : contours[i]) {
            circle(imcpy, point, 2, Scalar(100, 100, 100));
            std::cout<<point<<std::endl;
        }
        circle(imcpy, points.glass1, points.radius1, Scalar(100, 100, 100));
        circle(imcpy, points.glass2, points.radius2, Scalar(100, 100, 100));
        imshow("Imagecontour", imcpy);
        waitKey(-1);
*/
        vector<Point> hull;
        vector<int> defects;
        convexHull(contours[i], hull);

        vector<Point> defectsPoint = hull;//defectsHull(hull, contours[i]);
        if (inContour(points, defectsPoint))
        {
            return contours[i];
        }
    }

    std::cout<< "NO CONTOUR..."<<std::endl;
    return vector<Point>();
}

vector<Point> MainContourDetection::defectsHull(vector<int> points, vector<Point> contour)
{
    vector<Point> outPoints;
    vector<Point> contourPoints = contour;
    for(int point : points)
    {
        outPoints.push_back(contourPoints[point]);
    }


    return outPoints;
}

bool MainContourDetection::inContour(GlassesPoints points, vector<Point> c)
{
    double r1 = pointPolygonTest(c, points.glass1, true);
    double r2 = pointPolygonTest(c, points.glass2, true);

    return r1 > - points.radius1/4.0 &&
        r2 > - points.radius2/4.0;
}

GlassesPoints MainContourDetection::FindGlasses(Mat grayImage)
{
    GlassesPoints points;

    vector<Vec3f> circles;
    HoughCircles(grayImage, circles, CV_HOUGH_GRADIENT, 4.0, 100.0);
    int maxRadius = 0;
    for (int i = 0; i < circles.size() - 1; i++)
    {
        Vec3f vecCircle = circles[i];
        Point p(vecCircle[0], vecCircle[1]);
        int r = (int) vecCircle[2];
        if (r < maxRadius)
            continue;

        for (int j = i + 1; j < circles.size(); j++)
        {
            Vec3f vecCircle2 = circles[j];
            int r2 = (int) vecCircle2[2];
            int radiusDiff = abs(r - r2);
            double relativeRadiusDiff = 2*radiusDiff / (double)(r + r2);
            if (!Between(relativeRadiusDiff, 0.0, 0.6))
                continue;

            Point p2(vecCircle2[0], vecCircle2[1]);
            Point diff = p - p2;
            double distance = sqrt(diff.x*diff.x + diff.y + diff.y);
            double relativeDistance = distance / (double)(r + r2);
            if (Between(relativeDistance, 0.8, 2) && r2 > maxRadius) {
                circle(resultImg, p, r,  Scalar(5, 5, 25), 3);
                circle(resultImg, p2, r2, Scalar(5, 5, 25), 3);

                points.glass1 = p;
                points.radius1 = r;
                points.glass2 = p2;
                points.radius2 = r2;
                points.filled = true;

                maxRadius = max(r, r2);

                break;
            }
        }
    }

    return points;
}

bool MainContourDetection::Between(double value, double threshold1, double threshold2)
{
    return value >= threshold1 && value <= threshold2;
}

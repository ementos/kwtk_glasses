#include "GlassNormalizer.h"

GlassNormalizer::GlassNormalizer()
{
}

Mat GlassNormalizer::Normalize(vector<Point> mainContour, Mat grayImage)
{
    Mat returnImage = grayImage.clone();
    this->resultImg = returnImage;

    if (mainContour.size() <= 0)
    {
        std::cout << ("Couldn't find contur");
        return returnImage;
    }

    RotatedRect rect = minAreaRect(Mat(mainContour));

    /*
    Point2f vertices[4];
    rect.points(vertices);
    for(int i=0; i<4; ++i)
    {
        line(returnImage, vertices[i], vertices[(i+1)%4], Scalar(100,100,100));
    }*/

    Size rotatedSize = rect.size; // rect.center
    Point imageRotatePoint(0, 0);
    Mat rotationMatrix;
    if (rotatedSize.width > rotatedSize.height) {
        rotationMatrix = getRotationMatrix2D(imageRotatePoint, rect.angle, 1.0);
    } else {
        rotationMatrix = getRotationMatrix2D(imageRotatePoint, rect.angle + 90, 1.0);
        rotatedSize=Size(rect.size.height, rect.size.width);
    }

    Mat normalizedImage(returnImage.size(), returnImage.type());
    normalizedImage.setTo(255);

    std::vector<cv::Point2f> rect_corners_transformed = { rect.center };
    std::cout<<"Rect params:"<<rect.center<<std::endl;
    cv::Mat_<double> M(3, 1);
    M(0, 0) = rect.center.x;
    M(1, 0) = rect.center.y;
    M(2, 0) = 1.0;
    std::cout<<"Rect param:"<<M(0,0)<<std::endl;
    //fill M with affine transformation matrix
    //cv::transform(std::vector<cv::Point2f>(std::begin(rect_corners), std::end(rect_corners)), rect_corners_transformed, M);
    Mat_<double> Mp=rotationMatrix*M;//warpAffine(M, M, rotationMatrix, normalizedImage.size(), CV_INTER_LINEAR, BORDER_TRANSPARENT);
    std::cout<<"Rect param:"<<Mp(0,0)<<std::endl;
    std::cout<<"Rect param:"<<Mp(1,0)<<std::endl;
    rect.center = Point2f(Mp(0,0), Mp(1, 0));

    warpAffine(returnImage, normalizedImage, rotationMatrix, normalizedImage.size(), CV_INTER_LINEAR, BORDER_TRANSPARENT);

    getRectSubPix(normalizedImage, rotatedSize, rect.center, normalizedImage);

    return normalizedImage;
}

bool GlassNormalizer::Between(double value, double threshold1, double threshold2)
{
    return value > threshold1 && value < threshold2;
}

#include "TrainSetPreparer.h"
TrainSetPreparer::TrainSetPreparer()
{
}

void TrainSetPreparer::Prepare(std::string inputDirectoryName, std::string outDirectoryName)
{
    for (int i = 1; i < 10; i++) {
        std::string fileName = "glass_" + std::to_string(i) + ".jpg";
        std::string filePath = inputDirectoryName + fileName;
        Mat img;
        try {
            // Load image
            img = imread(filePath, CV_LOAD_IMAGE_COLOR);
            if (img.rows < 1) {
                continue;
            }
        }
        catch (Exception e)
        {
            continue;
        }

        Mat grayImage;
        cvtColor(img, grayImage, COLOR_BGR2GRAY);

        MainContourDetection mcDetector;
        vector<Point> mainContour = mcDetector.Detect(grayImage);

        GlassNormalizer normalizer;
        if (mainContour.size() <= 0)
        {
            std::cout << ("Couldn't find contour for " + fileName);
            continue;
        }

        Mat normalizedImage = normalizer.Normalize(mainContour, grayImage);

        std::string outFilePath = outDirectoryName + fileName;
        std::cout << "Saving file for " << outFilePath << std::endl;
        storedTrainFiles.push_back({outFilePath, i});
        imwrite(outFilePath, normalizedImage);
    }
}

void TrainSetPreparer::Prepare(std::vector<std::string> inputFiles, std::string outDirectoryName)
{
    for (int i = 0; i < inputFiles.size(); i++) {
        std::string fileName = "glass_" + std::to_string(i) + ".jpg";
        std::string outFilePath = outDirectoryName + fileName;
        std::string filePath = inputFiles[i];
        //std::cout<<"Preparing file "<<filePath<<std::endl;
        // Run only for not saved files
        struct stat buffer;
        if (stat (outFilePath.c_str(), &buffer) != 0) {

            try {

            Mat img;
            try {
                // Load image
                img = imread(filePath, CV_LOAD_IMAGE_COLOR);
                Mat gray_img = imread(filePath, CV_LOAD_IMAGE_GRAYSCALE);
                if (countNonZero(gray_img) < 100)
                {
                    continue;
                }
                if (img.rows < 1) {
                    continue;
                }
            }
            catch (Exception e)
            {
                continue;
            }

            Mat grayImage;
            cvtColor(img, grayImage, COLOR_BGR2GRAY);

            MainContourDetection mcDetector;
            vector<Point> mainContour = mcDetector.Detect(grayImage);

            GlassNormalizer normalizer;
            if (mainContour.size() <= 0)
            {
                std::cout << ("Couldn't find contour for " + fileName);
                continue;
            }

            Mat normalizedImage = normalizer.Normalize(mainContour, grayImage);
            imwrite(outFilePath, normalizedImage);
            std::cout << "Saving file for " << outFilePath << std::endl;

        } catch(...) {
            continue;
        }
        }
        storedTrainFiles.push_back({outFilePath, i});
    }
}

#include <cv.h>
#include <highgui.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include "EigenFacesProcess.h"

using namespace std;
using namespace cv;

int main(int argc, char *argv[])
{
    char* imageName = argv[1];

    // Show the process

    Mat image = imread( imageName, 1 );

    printf( "Start \n " );
    if( argc != 2 || !image.data )
    {
        cout << "No image data" << endl;
        return -1;
    }

    EigenFacesProcess eigenFacesProcess;
    eigenFacesProcess.RunProcess(image);

    waitKey(-1);

    return 0;
}

#include <cv.h>
#include <highgui.h>
#include <opencv2/imgproc/imgproc.hpp>

#include "MainContourDetection.h"

class ContourDetectionProcess
{
public:
    ContourDetectionProcess ();
    virtual ~ContourDetectionProcess ();
    vector<Point> RunProcess(Mat imageToDetect);

private:
    MainContourDetection mcDetector;
};

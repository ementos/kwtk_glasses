#ifndef TRAIN_SET_PREPARER
#define TRAIN_SET_PREPARER

#include <cv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "MainContourDetection.h"
#include "GlassNormalizer.h"

using namespace cv;
using namespace std;

class TrainSetPreparer
{
public:
    TrainSetPreparer ();
    void Prepare(std::vector<std::string> inputFiles, std::string outDirectoryName);
    virtual ~TrainSetPreparer () {};
    vector<std::pair<std::string, int>> storedTrainFiles;

private:
    /* data */
    void Prepare(std::string inputDirectoryName, std::string outDirectoryName);
};

#endif

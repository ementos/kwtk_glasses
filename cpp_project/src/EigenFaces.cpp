#include "EigenFaces.h"

JNIEXPORT void JNICALL Java_main_EigenfacesConnector_svmNormalize (JNIEnv * env, jobject jobj, jstring binaryPath, jstring imagePath, jstring normalizedPath)
{
    const char* binaryName = env->GetStringUTFChars(binaryPath, NULL) ;
    const char* imageName = env->GetStringUTFChars(imagePath, NULL) ;
    const char* normalizedName = env->GetStringUTFChars(normalizedPath, NULL) ;

    Mat binaryImage = imread(binaryName, 0);
    Mat image = imread(imageName, 0);

    // Normalize
    MainContourDetection mcDetector;
    vector<Point> mainContour = mcDetector.Detect(binaryImage);

    GlassNormalizer normalizer;
    Mat normalized = normalizer.Normalize(mainContour, image);
    imwrite(normalizedName, normalized);
}

JNIEXPORT jintArray JNICALL Java_main_EigenfacesConnector_svmPredict (JNIEnv * env, jobject jobj, jstring path, jobjectArray trainingFiles)
{
    const char* fileName = env->GetStringUTFChars(path, NULL) ;
    int stringCount = env->GetArrayLength(trainingFiles);

    std::vector<std::string> trainingFilesStrings;
    for (int i=0; i<stringCount; i++) {
        jstring string = (jstring) (env->GetObjectArrayElement(trainingFiles, i));
        const char *rawString = env->GetStringUTFChars(string, 0);
        trainingFilesStrings.push_back(rawString);
    }

    EigenFaces eigenFaces(trainingFilesStrings);
    Mat image = imread(fileName, 0);
    std::vector<int> predictedLabels;

    predictedLabels.push_back(eigenFaces.Predict(image));

    std::cout<<".";
    if (eigenFaces.TrainWithout(predictedLabels)){
    predictedLabels.push_back(eigenFaces.Predict(image));

    std::cout<<".";
    if(eigenFaces.TrainWithout(predictedLabels))
    predictedLabels.push_back(eigenFaces.Predict(image));

    std::cout<<".";
    if(eigenFaces.TrainWithout(predictedLabels))
    predictedLabels.push_back(eigenFaces.Predict(image));

    std::cout<<".";
    if(eigenFaces.TrainWithout(predictedLabels))
    predictedLabels.push_back(eigenFaces.Predict(image));

    std::cout<<".";
    if(eigenFaces.TrainWithout(predictedLabels))
    predictedLabels.push_back(eigenFaces.Predict(image));

    std::cout<<".";
    if(eigenFaces.TrainWithout(predictedLabels))
    predictedLabels.push_back(eigenFaces.Predict(image));

    std::cout<<"."<<std::endl;
    if (eigenFaces.TrainWithout(predictedLabels))
    predictedLabels.push_back(eigenFaces.Predict(image));
    }

	jintArray ret = (jintArray)env->NewIntArray(predictedLabels.size());
    jint returnValues[predictedLabels.size()];

	int i = 0;
	for(auto label : predictedLabels) {
        returnValues[i++] = label;
    }
    env->SetIntArrayRegion(ret, 0, predictedLabels.size(), returnValues);

	return ret;
}

EigenFaces::EigenFaces(std::vector<std::string> trainingFilesStrings)
{
    preparer.Prepare(trainingFilesStrings, "res/trained/");
    Train();
}

/*
EigenFaces::EigenFaces()
{
    //preparer.Prepare("res/kodano/", "res/trained/");
    //Train();
}*/

EigenFaces::~EigenFaces()
{
}

static Mat norm_0_255(InputArray _src) {
    Mat src = _src.getMat();
    // Create and return normalized image:
    Mat dst;
    switch(src.channels()) {
    case 1:
        cv::normalize(_src, dst, 0, 255, NORM_MINMAX, CV_8UC1);
        break;
    case 3:
        cv::normalize(_src, dst, 0, 255, NORM_MINMAX, CV_8UC3);
        break;
    default:
        src.copyTo(dst);
        break;
    }
    return dst;
}

void EigenFaces::Train()
{
    model = createEigenFaceRecognizer();

    // Read from directory

    images_train.clear();
    labels_train.clear();

    for (auto trainElement : preparer.storedTrainFiles)
    {
        Mat image = imread(trainElement.first, 0 );
        resize(image, image, Size(100, 100));

        if (!image.empty())
        {
            //cv::normalize(image, image, 0, 255, NORM_MINMAX, CV_8UC1);
            images_train.push_back(image);
            labels_train.push_back(trainElement.second);
        }
    }

    struct stat buffer;
    if (stat ("Eigenfaces.trainfile", &buffer) != 0) {
        model->train(images_train, labels_train);
        model->save("Eigenfaces.trainfile");
    } else {
        model->load("Eigenfaces.trainfile");
    }

    //Mat mean = model->getMat("mean");
    //imshow("Eigenfaces", norm_0_255(mean.reshape(1, images[0].rows)));
}

bool EigenFaces::TrainWithout(std::vector<int> without)
{
    model = createEigenFaceRecognizer();
    // Read from directory

    vector<Mat> images;
    vector<int> labels;

    for (int i = labels_train.size() - 1; i >= 0; i--)
    {
        auto trainLabel = labels_train[i];
        if (std::find(without.begin(), without.end(), trainLabel) != without.end())
        {
            labels_train.erase(labels_train.begin() + i);
            images_train.erase(images_train.begin() + i);
        }
    }

    try {
        model->train(images_train, labels_train);
    }catch(cv::Exception e) {
        std::cout<<"Training without failed with"<<std::endl;
        std::cout<<"Size: "<<labels_train.size()<<std::endl;
        return false;
    }

    return true;
}


int EigenFaces::Predict(Mat image)
{
    resize(image, image, Size(100, 100));

    return model->predict(image);
}

std::vector<std::string> EigenFaces::Predict(std::string filePath)
{
    Mat image = imread(filePath, 0);

	int predictedLabel = Predict(image);
    std::vector<std::string> predictedLabels;
    for (auto imageLabelPair : preparer.storedTrainFiles) {
        if (imageLabelPair.second == predictedLabel) {
            predictedLabels.push_back(imageLabelPair.first);
        }
    }

    return predictedLabels;
}

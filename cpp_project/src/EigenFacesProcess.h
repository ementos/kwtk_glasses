#include <cv.h>
#include <highgui.h>
#include <opencv2/imgproc/imgproc.hpp>

#include "ContourDetectionProcess.h"
#include "GlassNormalizer.h"
#include "EigenFaces.h"
#include "TrainSetPreparer.h"

class EigenFacesProcess
{
public:
    EigenFacesProcess();
    void RunProcess(Mat image);
    virtual ~EigenFacesProcess ();

private:
    TrainSetPreparer preparer;
//    EigenFaces eigenFaces;
    ContourDetectionProcess contourDetectionProcess;
};

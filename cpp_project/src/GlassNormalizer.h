#include <cv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>

using namespace cv;
using namespace std;

#ifndef GLASS_NORMALIZER_H
#define GLASS_NORMALIZER_H

class GlassNormalizer
{
public:
    GlassNormalizer();
    Mat Normalize(vector<Point> mainContour, Mat grayImage);
    virtual ~GlassNormalizer () {};
    static bool Between(double value, double threshold1, double threshold2);

private:
    vector<Point> mainContour;

    Mat resultImg;
};

#endif

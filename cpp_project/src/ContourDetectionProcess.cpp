#include "ContourDetectionProcess.h"

ContourDetectionProcess::ContourDetectionProcess() {}
ContourDetectionProcess::~ContourDetectionProcess() {}

vector<Point> ContourDetectionProcess::RunProcess(Mat imageToDetect)
{
    vector<Point> mainContour = mcDetector.Detect(imageToDetect);

    // In future possible contour manuall correct
    auto color = Scalar(122, 120, 255);
    Mat contour_imageToDetect(imageToDetect.rows, imageToDetect.cols, CV_8UC3, Scalar(0, 0, 0));
    for (int i = 0; i < mainContour.size(); i++)
    {
        line(contour_imageToDetect, mainContour[i - 1], mainContour[i], color);
    }

    circle(contour_imageToDetect, mcDetector.glassesPoints.glass1, mcDetector.glassesPoints.radius1, color);
    circle(contour_imageToDetect, mcDetector.glassesPoints.glass2, mcDetector.glassesPoints.radius2, color);
    imshow("Detected contour", contour_imageToDetect);
    waitKey(-1);

    return mainContour;
}


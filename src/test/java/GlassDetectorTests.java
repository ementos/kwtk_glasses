import static org.junit.Assert.assertEquals;

import org.junit.Test;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;

public class GlassDetectorTests {

    @Test
    public void checkOpenCVSupport() {
        assertEquals("OpenCV version must be than 2.4.9.1", "2.4.9.1", Core.VERSION);
        assertEquals("Library name must be ...", "opencv_java249", Core.NATIVE_LIBRARY_NAME);
    }
}

package main;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.*;
import org.opencv.highgui.*;
import java.util.*;

public class MainContourDetection {

    GlassesPoints glassesPoints;

    MatOfPoint mainContour;

    MainContourDetection()
    {
    }

    Mat resultImg;

    public MatOfPoint Detect(Mat grayImage)
    {
        Mat returnImage = grayImage.clone();
        this.resultImg = returnImage;

        Mat workingImage = grayImage.clone();

        glassesPoints = FindGlasses(workingImage);
        if (glassesPoints == null)
        {
            System.out.println("Couldn't find circles");
            return null;
        }

        workingImage = grayImage.clone();

        return GetContourAssociatedWithGlasses(workingImage, glassesPoints);
    }

    MatOfPoint GetContourAssociatedWithGlasses(Mat grayImage, GlassesPoints points)
    {
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat binaryImage = new Mat();
        Imgproc.Canny(grayImage, binaryImage, 10, 100, 3, true);
        Imgproc.findContours(binaryImage, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        for(int i=0; i < contours.size();i++) {
            MatOfInt hull = new MatOfInt();
            MatOfInt4 defects = new MatOfInt4();
            Imgproc.convexHull(contours.get(i), hull);

            MatOfPoint defectsPoint = defectsHull(hull.toArray(), contours.get(i));
            if (inContour(points.glass1, defectsPoint) &&
                inContour(points.glass2, defectsPoint))
            {
                return contours.get(i);
            }
        }

        return null;
    }

    MatOfPoint defectsHull(int[] points, MatOfPoint contour)
    {
        List<Point> outPoints = new ArrayList<Point>();
        Point[] contourPoints = contour.toArray();
        for(int point : points)
        {
            outPoints.add(contourPoints[point]);
        }
 

        MatOfPoint pointsVector = new MatOfPoint();
        pointsVector.fromList(outPoints);

        return pointsVector;
    }

    boolean inContour(Point p, MatOfPoint c)
    {
        MatOfPoint2f m = new MatOfPoint2f(c.toArray());

        double r = Imgproc.pointPolygonTest(m, p, false);

        return r >= 0;
    }

    private GlassesPoints FindGlasses(Mat grayImage)
    {
        GlassesPoints points = null;

        Mat circles = new Mat();
        Imgproc.HoughCircles(grayImage, circles, Imgproc.CV_HOUGH_GRADIENT, 4.0, 100.0);
        int maxRadius = 0;
        for (int i = 0; i < circles.cols() - 1; i++)
        {
            double[] vecCircle = circles.get(0, i);
            Point p = new Point(vecCircle[0], vecCircle[1]);
            int r = (int) vecCircle[2];
            if (r < maxRadius)
                continue;

            for (int j = i + 1; j < circles.cols(); j++)
            {
                double[] vecCircle2 = circles.get(0, j);
                int r2 = (int) vecCircle2[2];
                int radiusDiff = Math.abs(r - r2);
                double relativeRadiusDiff = 2*radiusDiff / (double)(r + r2);
                System.out.println("Radiuses " + r + ","+ r2);
                System.out.println("RelativeRadiusDiff " + relativeRadiusDiff);
                System.out.println("RadiusDiff " + radiusDiff);
                if (!Between(relativeRadiusDiff, 0.0, 0.6))
                    continue;
                
                Point p2 = new Point(vecCircle2[0], vecCircle2[1]);
                Point diff = new Point(p.x - p2.x, p.y - p2.y);
                double distance = Math.sqrt(diff.x*diff.x + diff.y + diff.y);
                double relativeDistance = 2*distance / (r + r2);
                if (Between(relativeDistance, 2, 3) && r2 > maxRadius) {
                    Core.circle(resultImg, p, r, new Scalar(5, 5, 25), 3);
                    Core.circle(resultImg, p2, r2, new Scalar(5, 5, 25), 3);

                    points = new GlassesPoints();
                    points.glass1 = p;
                    points.radius1 = r;
                    points.glass2 = p2;
                    points.radius2 = r2;

                    maxRadius = Math.max(r, r2);

                    break;
                }
            }
        }

        return points;
    }

    private static boolean Between(double value, double threshold1, double threshold2)
    {
        return value > threshold1 && value < threshold2;
    }
}


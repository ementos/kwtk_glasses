package main;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.*;
import org.opencv.highgui.*;
import java.util.*;
import org.opencv.utils.Converters;


public class GlassDetector {
    
    String filePath;
    
    GlassDetector(String s)
    {
        filePath = s;
    }
    
    String preprocess(int value, boolean isValueSet)
    {
        Mat img = Highgui.imread(filePath, Highgui.CV_LOAD_IMAGE_COLOR);
        Mat grayImage = new Mat();
        Imgproc.cvtColor(img, grayImage, Imgproc.COLOR_BGR2GRAY);
        GlassTemplesRemover gtRemover = new GlassTemplesRemover();
        Mat withoutTemples; 
        if (isValueSet==false)
            withoutTemples = gtRemover.removeTemples(grayImage);
        else 
            withoutTemples = gtRemover.removeTemples(grayImage, value);
        testFourier(gtRemover, img);
        String fileName = "tmpOla.jpg";
        Highgui.imwrite(fileName, withoutTemples);
        
        return fileName;
    }
    void extractFourier(GlassTemplesRemover gtRemover, Mat img){
        MatOfPoint contour = gtRemover.getGlassesContour().get(0);
        System.out.println("na poczatku liczba kanalow "+contour.channels());
        List<Point> list = new ArrayList<Point>();
        Converters.Mat_to_vector_Point(contour, list);
        int n = list.size();
        double[] x = new double[n];
	double[] y = new double[n];
        for (int i = 0; i <  n;i++){
            x[i]=list.get(i).x;
            y[i]=list.get(i).y;
            //System.out.println("przed "+x[i]+" "+y[i]);
        }				
	EllipticFourierDescriptor fourier = new EllipticFourierDescriptor(x, y, 150);
        double[] descriptors = fourier.getEFD();
        Connection conn = Connection.getInstance();

        //conn.getInstance().insertDescriptors(descriptors);
    }
    void testFourier(GlassTemplesRemover gtRemover, Mat img)
    {
        //ekstrakcja deskryptorów Fouriera z największego konturu
        MatOfPoint contour = gtRemover.getGlassesContour().get(0);
        System.out.println("na poczatku liczba kanalow "+contour.channels());
        List<Point> list = new ArrayList<Point>();
        Converters.Mat_to_vector_Point(contour, list);
        int n = list.size();
        double[] x = new double[n];
	double[] y = new double[n];
        for (int i = 0; i <  n;i++){
            x[i]=list.get(i).x;
            y[i]=list.get(i).y;
            //System.out.println("przed "+x[i]+" "+y[i]);
        }				
	EllipticFourierDescriptor fourier = new EllipticFourierDescriptor(x, y, 150);
        
        //double[] efds =fourier.getEFD();
        
        //odwrotne przekształcenie: deksryptorów w kontur zeby sprawdzic
        int[][] invert = fourier.createPolygonInt();
        //System.out.println(invert.toString());
        List<Point> invertList = new ArrayList<Point>();
        for (int i = 0; i <  n;i++){
            Point pp = new Point(invert[i][0],invert[i][1]);
            invertList.add(pp);
           // System.out.println("po "+pp.x+" "+pp.y);
        }      
        MatOfPoint end = new MatOfPoint();
        end.fromList(invertList);
        Mat contourImg = new Mat(end.rows(),end.cols(),0,new Scalar(255,255,255));
        Imgproc.resize(contourImg,contourImg,img.size() );
        Scalar s = new Scalar(0, 255, 0);
        List<MatOfPoint> lista = new ArrayList<MatOfPoint>();
        lista.add(end);
        Imgproc.drawContours(contourImg, lista, 0, s, -1);
        //.out.println("liczba kanałów "+ contourImg.channels());
        Highgui.imwrite("tmpMytych.jpg", contourImg);
    }
    
    public static void main(String args[]) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
    }
    

    /*
    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        String filePath =  "res/image18.jpg";

        // Load image
        Mat img = Highgui.imread(filePath, Highgui.CV_LOAD_IMAGE_COLOR);
        Mat grayImage = new Mat();
        Imgproc.cvtColor(img, grayImage, Imgproc.COLOR_BGR2GRAY);
        
        // usuwanie zausznikĂłw
        GlassTemplesRemover gtRemover = new GlassTemplesRemover();
        Mat withoutTemples = gtRemover.removeTemples(grayImage);
        Highgui.imwrite("tmpOla.jpg", withoutTemples);
        //
        
        
        MainContourDetection mcDetector = new MainContourDetection();
        MatOfPoint mainContour = mcDetector.Detect(withoutTemples);
        Highgui.imwrite("detectedGlasses.jpg", mcDetector.resultImg);

        //Temple remover doesn't work yet, so it's not used by now, however it produces output image for testing purposes
        //TemplesRemover templesRemover = new TemplesRemover();
        //MatOfPoint contourWithoutTemples = templesRemover.GetTemplesContour(mainContour, grayImage, mcDetector.glassesPoints);
        //List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        //contours.add(contourWithoutTemples);
        //Imgproc.drawContours(img, contours, 0, new Scalar(0, 0, 255));
        //Highgui.imwrite("detectedGlasses2.jpg", img);

        //GlassNormalizer normalizer = new GlassNormalizer(new Size(250,150));
        //Mat normalizedImage = normalizer.Normalize(mainContour, grayImage);
        
        //Segmentation seg = new Segmentation();
        //Mat foregroundImage = seg.doBackgroundRemoval(img);
        //Highgui.imwrite("tmp.jpg", foregroundImage);

        //Highgui.imwrite("tmp.jpg", normalizedImage);
                
    }*/
}


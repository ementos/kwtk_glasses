package main;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import static main.GlassTemplesRemover.thresVal;

public class GlassDetailsView extends javax.swing.JFrame {
    JPanel galleryPanel;
    String filePath;
    Connection connection;
    Model model;

    public GlassDetailsView(Model model) {
        this.model = model;
        connection = Connection.getInstance();
        setMinimumSize(new java.awt.Dimension(840, 480));
        initComponents();
    }

    private void initComponents() {
        final JPanel windowPanel = new JPanel();
        windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.Y_AXIS));

        galleryPanel = new JPanel();
        GridLayout galleryLayout = new GridLayout(0, 3, 2, 2);
        galleryPanel.setLayout(galleryLayout);
//        galleryPanel.setPreferredSize(new Dimension(620, 480));
        galleryPanel.setAutoscrolls(true);
        javax.swing.JLabel titleLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("Podgląd modelu " + model.name);

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        titleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleLabel.setText("Model: " + model.name);
        titleLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        windowPanel.add(titleLabel);

        final JScrollPane galleryScrollbar = new JScrollPane(galleryPanel);
        add(galleryScrollbar, BorderLayout.PAGE_START);

        windowPanel.add(galleryScrollbar);
        add(windowPanel);

        fillDetails();
    }

    private void fillDetails() {
        galleryPanel.removeAll();
        ArrayList<ModelInstance> allModels = connection.getAllModelInstances(model.id);
        for (ModelInstance model : allModels) {
            galleryPanel.add(new GlassInstanceComponent(model));
        }
    }
}

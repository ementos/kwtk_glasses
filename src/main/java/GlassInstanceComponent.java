package main;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;

public class GlassInstanceComponent extends JPanel {
    ModelInstance glass;
    public GlassInstanceComponent(ModelInstance glass) {
        this.glass = glass;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JLabel image_label = new JLabel(new ImageIcon(glass.file_name));
        add(image_label);
        add(new JLabel(glass.name));
    }
}

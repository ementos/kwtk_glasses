package main;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.*;
import org.opencv.highgui.*;
import java.util.*;

public class TemplesRemover {

    TemplesRemover()
    {
    }

    public MatOfPoint RemoveTemples(MatOfPoint mainContour, Mat grayImage, GlassesPoints glassesPoints)
    {
        List<Point> points = FindContourPoints(mainContour, glassesPoints);
        if (points == null)
            return mainContour;
        for (Point point : points) {
            Core.circle(grayImage, point, 3, new Scalar(225, 225, 225));
        }

        MatOfPoint contour = new MatOfPoint();
        contour.fromList(points);

        return contour;
    }

    public MatOfPoint GetTemplesContour(MatOfPoint mainContour, Mat grayImage, GlassesPoints glassesPoints)
    {
        List<Point> points = FindContourPoints(mainContour, glassesPoints);
        if (points == null)
            return mainContour;

        MatOfPoint contour = new MatOfPoint();
        contour.fromList(points);
        for (Point point : points) {
            // Core.circle(grayImage, point, 3, new Scalar(225, 225, 225));
        }

        return contour;
    }

    public List<Point> FindContourPoints(MatOfPoint contour, GlassesPoints glassesPoints)
    {
        List<Point> suspectedPoints = new ArrayList<Point>();
        if (contour == null)
            return null;
        Point[] points = contour.toArray();
        double radiusSum = (glassesPoints.radius1 + glassesPoints.radius2);
        for (int j = 0; j < points.length; j++) {
            if (HasSelfContourAround(j, points))
            {
                if (distance(points[j], glassesPoints.GetMiddlePoint()) < radiusSum/2) {
                    continue;
                }
            }
            suspectedPoints.add(points[j]);
        }

        return suspectedPoints;
    }

    public boolean HasSelfContourAround(int pointId, Point[] points)
    {
        double minSize = 10;
        Point point = points[pointId];
        if (points.length < pointId + 2)
            return false;

        Point nextPoint = points[pointId + 1];
        Point oppositeContourVector = new Point(nextPoint.x - point.x, nextPoint.y - point.y);
        for (int i = 1; i < points.length; i++) {
            if (Math.abs(i - pointId) < 3)
            {
                continue;
            }


            Point currentPoint = points[i];
            Point previousPoint = points[i - 1];

            Point contourVector = new Point(currentPoint.x - previousPoint.x, currentPoint.y - previousPoint.y);
            Point correspondenceVector = new Point(currentPoint.x - point.x, currentPoint.y - point.y);
            if (LineBetweenPoints(contourVector, correspondenceVector, 0.0, 0.2)) {
                continue;
            }

            if (LineBetweenPoints(contourVector, oppositeContourVector, 0.93, 1.0)) {
                continue;
            }

            double distance = distance(new Point(point.x - currentPoint.x, point.y - currentPoint.y));

            // Remove noise
            if (distance <= 3) {
                continue;
            }

            if (distance <= minSize) {
                return true;
            }
        }

        return false;
    }

    boolean LineBetweenPoints(Point v1, Point v2, double minValue, double maxValue)
    {
        double lengthsMultiply = distance(v1) * distance(v2);
        double dotP = DotFromTwoVectors(v1, v2);
        if (lengthsMultiply != 0 &&
                Math.abs(dotP/lengthsMultiply) > minValue &&
                Math.abs(dotP/lengthsMultiply) < maxValue)
        {
            return true;
        }

        return false;
    }

    double DotFromTwoVectors(Point v1, Point v2)
    {
        return v1.dot(v2);
    }


    static double distance(Point point) {
        return Math.sqrt(point.x * point.x + point.y * point.y);
    }

    static double distance(Point point1, Point point2) {
        Point diff = new Point(point1.x - point2.x, point1.y - point2.y);
        return distance(diff);
    }
    
}

package main;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.*;
import org.opencv.highgui.*;
import java.util.*;

class GlassesPoints {
    Point glass1;
    double radius1;
    Point glass2;
    double radius2;

    Point GetMiddlePoint()
    {
        return new Point((glass1.x + glass2.x)/2, (glass1.y + glass2.y)/2);
    }
}

package main;

import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.nio.file.*;
import java.nio.file.Files;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.SwingConstants;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import static main.ResultsFrame.list;

public class NewJFrame extends javax.swing.JFrame {

    public static String filePath;
    ArrayList<main.Model> models = new ArrayList<>();
    boolean nowyModel = false;
    
    public NewJFrame() {
        initComponents();
        jInternalFrame2.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
        jInternalFrame2.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        ((BasicInternalFrameUI) jInternalFrame2.getUI()).setNorthPane(null);
        
        jInternalFrame3.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
        jInternalFrame3.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        ((BasicInternalFrameUI) jInternalFrame3.getUI()).setNorthPane(null);

        jSlider1.setValue(0);
        jTextField2.setText(String.valueOf(jSlider1.getValue()));

    }

    @SuppressWarnings("unchecked")
    
    float getScale(BufferedImage image, JInternalFrame jFrame)
    {
        float scale=1f;
        
        if (image.getWidth()>jFrame.getWidth() || image.getHeight()>jFrame.getHeight())
        {
            if (image.getWidth()>jFrame.getWidth() && image.getHeight()<jFrame.getHeight())
                scale = (float)(jFrame.getWidth())/(float)((image.getWidth()+2)); 
            else if (image.getHeight()>jFrame.getHeight() && image.getWidth()<jFrame.getWidth())
                scale = (float)(jFrame.getHeight())/(float)((image.getHeight()+2));  
            else 
            {
                float s1 = (float)(jFrame.getWidth())/(float)((image.getWidth()+2)); 
                float s2 = (float)(jFrame.getHeight())/(float)((image.getHeight()+2));
                if (s1>s2) scale = s2; 
                else scale = s1;
            }
        }
        return scale;
    }
    
    public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }
        return "";
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jInternalFrame2 = new javax.swing.JInternalFrame();
        jButton2 = new javax.swing.JButton();
        jInternalFrame3 = new javax.swing.JInternalFrame();
        jSlider1 = new javax.swing.JSlider();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Identyfikacja modeli okularów");
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMaximumSize(new java.awt.Dimension(1167, 600));
        setMinimumSize(new java.awt.Dimension(1167, 600));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Identyfikacja modeli okularów");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jButton1.setText("Załaduj zdjęcie");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jInternalFrame2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jInternalFrame2.setAutoscrolls(true);
        jInternalFrame2.setFrameIcon(null);
        jInternalFrame2.setMaximumSize(new java.awt.Dimension(459, 384));
        jInternalFrame2.setMinimumSize(new java.awt.Dimension(459, 384));
        jInternalFrame2.setName(""); // NOI18N
        jInternalFrame2.setPreferredSize(new java.awt.Dimension(459, 384));
        jInternalFrame2.setVerifyInputWhenFocusTarget(false);
        jInternalFrame2.setVisible(true);

        javax.swing.GroupLayout jInternalFrame2Layout = new javax.swing.GroupLayout(jInternalFrame2.getContentPane());
        jInternalFrame2.getContentPane().setLayout(jInternalFrame2Layout);
        jInternalFrame2Layout.setHorizontalGroup(
            jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 457, Short.MAX_VALUE)
        );
        jInternalFrame2Layout.setVerticalGroup(
            jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 362, Short.MAX_VALUE)
        );

        jButton2.setText("Przetwarzanie wstępne");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jInternalFrame3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jInternalFrame3.setAutoscrolls(true);
        jInternalFrame3.setFrameIcon(null);
        jInternalFrame3.setMaximumSize(new java.awt.Dimension(459, 384));
        jInternalFrame3.setMinimumSize(new java.awt.Dimension(459, 384));
        jInternalFrame3.setName(""); // NOI18N
        jInternalFrame3.setPreferredSize(new java.awt.Dimension(459, 384));
        jInternalFrame3.setVerifyInputWhenFocusTarget(false);
        jInternalFrame3.setVisible(true);

        javax.swing.GroupLayout jInternalFrame3Layout = new javax.swing.GroupLayout(jInternalFrame3.getContentPane());
        jInternalFrame3.getContentPane().setLayout(jInternalFrame3Layout);
        jInternalFrame3Layout.setHorizontalGroup(
            jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 457, Short.MAX_VALUE)
        );
        jInternalFrame3Layout.setVerticalGroup(
            jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 362, Short.MAX_VALUE)
        );

        jSlider1.setMaximum(200);
        jSlider1.setVerifyInputWhenFocusTarget(false);
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });

        jLabel5.setText(" 0                           100                       200");

        jLabel3.setText("Wartość parametru:");

        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField2.setEnabled(false);
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jButton3.setText("Identyfikuj model");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Dobór wartości parametru");

        jButton4.setText("OK");
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton4MouseClicked(evt);
            }
        });

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setSelected(true);
        jRadioButton1.setText("deskryptory Fouriera");

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Eigenfaces");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Wybór metody");

        jButton5.setText("Przeglądaj bazę");
        jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton5MouseClicked(evt);
            }
        });

        jButton6.setText("Zarejestruj nowy model");
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton6MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jInternalFrame2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 459, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)
                    .addComponent(jInternalFrame3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSlider1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField2)))
                    .addComponent(jRadioButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jRadioButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jInternalFrame2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jInternalFrame3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(jButton3)
                            .addComponent(jButton5)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jRadioButton1)
                        .addGap(18, 18, 18)
                        .addComponent(jRadioButton2)
                        .addGap(29, 29, 29)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jButton4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(jButton6)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        final JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", "jpg", "jpeg", "gif", "png", "bmp", "tga");
        fc.setFileFilter(filter);
        int returnVal = fc.showOpenDialog(this);
        if (returnVal != JFileChooser.APPROVE_OPTION) return;
        File file = fc.getSelectedFile();

        filePath = file.getAbsolutePath();
        nowyModel = false;

        BufferedImage image = null;
        try
        {
            image = ImageIO.read(new File(filePath));
        }
        catch (Exception e)
        {
            LLogger.getLogger().warning("File "+filePath+" doesn't exist");
            JOptionPane.showMessageDialog(null, "Podany plik nie istnieje");
            return;
        }
        LLogger.getLogger().info("Loaded image "+filePath);

        try {
            jInternalFrame2.setVisible(false);
            jInternalFrame2.setSelected(false);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        float scale=getScale(image, jInternalFrame2);
        
        ImageIcon imageIcon = new ImageIcon(image.getScaledInstance((int)(image.getWidth()*scale), (int)(image.getHeight()*scale), 0));
        JLabel jLabel = new JLabel(imageIcon, SwingConstants.CENTER);
        jInternalFrame2.setContentPane(jLabel);
        try {
            jInternalFrame2.setVisible(true);
            jInternalFrame2.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }//GEN-LAST:event_jButton1MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged
        //System.out.println(jSlider1.getValue());
        jTextField2.setText(String.valueOf(jSlider1.getValue()));
    }//GEN-LAST:event_jSlider1StateChanged

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        
        nowyModel = false;
        String file;
        GlassDetector gd = new GlassDetector(filePath);
        try{
        file = gd.preprocess(0, false);
        }
        catch (Exception e)
        {
            LLogger.getLogger().warning("Preprocessing failed");
            JOptionPane.showMessageDialog(null, "Przetwarzanie nieudane");
            return;
        }
        
        try {
            jInternalFrame3.setVisible(false);
            jInternalFrame3.setSelected(false);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        BufferedImage image = null;
        try
        {
          image = ImageIO.read(new File(file));
          nowyModel = true;
        }
        catch (Exception e)
        {
            LLogger.getLogger().warning("File " + file +" doesn't exist");
          JOptionPane.showMessageDialog(null, "Podany plik nie istnieje");
          return;
        }
        
        float scale=getScale(image, jInternalFrame3);
        
        ImageIcon imageIcon = new ImageIcon(image.getScaledInstance((int)(image.getWidth()*scale), (int)(image.getHeight()*scale), 0));
        JLabel jLabel = new JLabel(imageIcon, SwingConstants.CENTER);
        jInternalFrame3.setContentPane(jLabel);
        try {
            jInternalFrame3.setVisible(true);
            jInternalFrame3.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        LLogger.getLogger().info("Preprocessing completed");
        
        jTextField2.setText(String.valueOf(GlassTemplesRemover.thresVal));
        jSlider1.setValue(GlassTemplesRemover.thresVal);



    }//GEN-LAST:event_jButton2MouseClicked

    private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseClicked
        
        int paramVal = jSlider1.getValue();
        GlassDetector gd = new GlassDetector(filePath);
        String file = gd.preprocess(paramVal, true);
        
        try {
            jInternalFrame3.setVisible(false);
            jInternalFrame3.setSelected(false);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        BufferedImage image = null;
        try
        {
          image = ImageIO.read(new File(file));
        }
        catch (Exception e)
        {
            LLogger.getLogger().warning("Couldn't open file "+file);
          JOptionPane.showMessageDialog(null, "Podany plik nie istnieje");
          return;
        }
        
        float scale=getScale(image, jInternalFrame3);
        
        ImageIcon imageIcon = new ImageIcon(image.getScaledInstance((int)(image.getWidth()*scale), (int)(image.getHeight()*scale), 0));
        JLabel jLabel = new JLabel(imageIcon, SwingConstants.CENTER);
        jInternalFrame3.setContentPane(jLabel);
        try {
            jInternalFrame3.setVisible(true);
            jInternalFrame3.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jButton4MouseClicked

    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        
       list.clear();
        
        String name = getSelectedButtonText(buttonGroup1);
        System.out.println(name);

        if (name.equals("deskryptory Fouriera"))
        {
            list = new EllipticFourierDescriptor().getListOfMatches(EllipticFourierDescriptor.efd);
        }
        else 
        {
            EigenfacesConnector connector = new EigenfacesConnector();
            connector.Normalize("tmpOla.jpg", filePath, "outFile.jpg");
            String[] predictedFiles = connector.Predict("outFile.jpg");
            for (int i=0; i<predictedFiles.length; i++)
            {
                Model mm = Connection.getInstance().getModelInfo(predictedFiles[i]);
                list.add(new Result(200f, mm));
            }
        }
        
        if (list.size()>0)
        {
            ResultsFrame rf;
            try {
                rf = new ResultsFrame();
                rf.setLocationRelativeTo(null);
                rf.ustaw();
                rf.setVisible(true);
            } catch (InterruptedException ex) {
                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }           
        }
        else 
            JOptionPane.showMessageDialog(new NewJFrame(), "Brak wyników");

    }//GEN-LAST:event_jButton3MouseClicked

    private void jButton5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton5MouseClicked
        
        GlassesBrowser g = new GlassesBrowser();
        g.setVisible(true);
    }//GEN-LAST:event_jButton5MouseClicked

    private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseClicked
        
        if (nowyModel==true)
        {
            //String nazwaModelu = JOptionPane.showInputDialog("Wprowadź nazwę modelu: ", );
            String[] options = {"OK"};
            JPanel panel = new JPanel();
            JLabel lbl = new JLabel("Wprowadź nazwę modelu: ");
            JTextField txt = new JTextField(10);
            panel.add(lbl);
            panel.add(txt);
           int selectedOption = JOptionPane.showOptionDialog(null, panel, "", JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options , options[0]);

            if(selectedOption == 0)
            {
                String nazwaModelu = txt.getText();
                int id1 = Connection.getInstance().getLastModelId();
                int id2 = Connection.getInstance().getLastInstanceId();
                String path = "res/kodano/"+id1+"_"+id2+".jpg";
                try {
                    ImageIO.write(ImageIO.read(new File(filePath)),"jpg", new File(path));
                } catch (IOException ex) {
                    Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                Connection.getInstance().insertDescriptors(nazwaModelu, path);
                LLogger.getLogger().info("Registered new model: "+nazwaModelu+" with instance: " + path);
                try {
                Files.deleteIfExists(Paths.get("Eigenfaces.trainfile"));
                }catch (IOException e){
                    System.out.print(e);
                }
                JOptionPane.showMessageDialog(new NewJFrame(), "Zarejestrowano nowy model");
            }  
        }
        else
            JOptionPane.showMessageDialog(new NewJFrame(), "Aby zarejestrować model w bazie wczytaj jego zdjęcie i wykonaj przetwarzanie wstępne");
    

    }//GEN-LAST:event_jButton6MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JInternalFrame jInternalFrame2;
    private javax.swing.JInternalFrame jInternalFrame3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}

package main;

import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.*;
import org.opencv.highgui.*;
import java.util.*;

public class GlassTemplesRemover {
    
    List<MatOfPoint> glassesContour = new ArrayList<>();
    int thresholdValue=0;
    public static int thresVal;
    boolean found=false;
    
    GlassTemplesRemover()
    {}
    
    public List<MatOfPoint> getGlassesContour()
    {
        return this.glassesContour;
    }
   
    public boolean isGlassesFound()
    {
        return found;
    }
    
    Mat removeTemples(Mat img)
    {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat image;
        
        int counter=0;
       
       while (found!=true && counter<255)
       {
           counter=counter+1;
                   
           if (contours.size()>0)
               contours.clear();
           
           image = new Mat();
           
           Imgproc.threshold(img,image,thresholdValue,128,Imgproc.THRESH_TOZERO);
           
           Imgproc.threshold(image,image,50,255,Imgproc.THRESH_BINARY);
           
           
           Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
      
           
           Collections.sort(contours, (MatOfPoint lhs, MatOfPoint rhs) -> lhs.size().height > rhs.size().height ? -1 : (lhs.size().height < rhs.size().height ) ? 1 : 0);
           
            areGlassesFound(contours);
       }
       
       Mat contourImg = new Mat(img.rows(),img.cols(),0,new Scalar(255,255,255));
       Imgproc.resize(contourImg,contourImg,img.size() );
       
       if (found==true)
       {
            for (int i = 0; i < 3; i++) 
            {
                int c=0;
                if (i!=0) c=255;
                Scalar s = new Scalar(c, 255, 0);
                Imgproc.drawContours(contourImg, contours, i, s, -1);
                glassesContour.add(contours.get(i));
            }
            thresVal=thresholdValue;
       }
       
       return contourImg;
    }
    
    
    Mat removeTemples(Mat img, int thresholdVal)
    {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat image;
        
        int counter=0;
       
        if (contours.size()>0)
        contours.clear();
           
        image = new Mat();
           
        Imgproc.threshold(img,image,thresholdVal,128,Imgproc.THRESH_TOZERO);
           
        Imgproc.threshold(image,image,50,255,Imgproc.THRESH_BINARY);
           
        Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
         
        Collections.sort(contours, (MatOfPoint lhs, MatOfPoint rhs) -> lhs.size().height > rhs.size().height ? -1 : (lhs.size().height < rhs.size().height ) ? 1 : 0);
           
        Mat contourImg = new Mat(img.rows(),img.cols(),0,new Scalar(255,255,255));
        Imgproc.resize(contourImg,contourImg,img.size() );
       

        for (int i = 0; i < 3; i++) 
        {
            int c=0;
            if (i!=0) c=255;
            Scalar s = new Scalar(c, 255, 0);
            Imgproc.drawContours(contourImg, contours, i, s, -1);
            glassesContour.add(contours.get(i));
        }
       
       return contourImg;
    }
    
    
    void areGlassesFound(List<MatOfPoint> c)
    {
        if (c.size()>=3 && (c.get(1).size().height-c.get(2).size().height<0.1*c.get(1).size().height))
        {
            boolean b=true;
            
            for (int i=0; i<c.get(1).toList().size(); i++)
            {
                if(Imgproc.pointPolygonTest(new MatOfPoint2f(c.get(2).toArray()), c.get(1).toList().get(i), false) > 0)
                    b=false;
            }
            
            for (int i=0; i<c.get(1).toList().size(); i++)
            {
                if(Imgproc.pointPolygonTest(new MatOfPoint2f(c.get(0).toArray()), c.get(1).toList().get(i), false) <= 0)
                    b=false;
            }
            
            for (int i=0; i<c.get(2).toList().size(); i++)
            {
                if(Imgproc.pointPolygonTest(new MatOfPoint2f(c.get(0).toArray()), c.get(2).toList().get(i), false) <= 0)
                    b=false;
            }
             
            if (b==false)
            {
                thresholdValue=thresholdValue+10;
                found= false;
            }
            else
                found= true;
        }
        
        else
        {
            thresholdValue=thresholdValue+10;
            found= false;
        }
    }
    
}

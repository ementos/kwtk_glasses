package main;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicInternalFrameUI;


public class ResultsFrame extends javax.swing.JFrame {
    
    static List<Result> list = new ArrayList<>();
    int strona;
    int liczbaStron;
    private HashMap componentMap;


    public ResultsFrame() throws InterruptedException {
        initComponents();
        
        jInternalFrame3.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
        jInternalFrame3.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        ((BasicInternalFrameUI) jInternalFrame3.getUI()).setNorthPane(null);
        jInternalFrame4.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
        jInternalFrame4.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        ((BasicInternalFrameUI) jInternalFrame4.getUI()).setNorthPane(null);
        jInternalFrame5.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
        jInternalFrame5.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        ((BasicInternalFrameUI) jInternalFrame5.getUI()).setNorthPane(null);
        jInternalFrame6.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
        jInternalFrame6.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        ((BasicInternalFrameUI) jInternalFrame6.getUI()).setNorthPane(null);
        
        jInternalFrame3.setName("f3");
        jInternalFrame4.setName("f4");
        jInternalFrame5.setName("f5");
        jInternalFrame6.setName("f6");
        
        createComponentMap();

        
    }
    
    void ustaw()
    {
        liczbaStron = (int)Math.ceil(Float.valueOf(String.valueOf(list.size()))/4f);
        setText("1/"+liczbaStron);
        strona = 1;
        wstawWyniki(strona);
    }
    
    private void createComponentMap() {
        componentMap = new HashMap<>();
        Component[] components = this.getContentPane().getComponents();
        for (int i=0; i < components.length; i++) {
                componentMap.put(components[i].getName(), components[i]);
        }
    }

    public Component getComponentByName(String name) {
        if (componentMap.containsKey(name)) {
                return (Component) componentMap.get(name);
        }
        else return null;
    }
    
    float getScale(BufferedImage image, JInternalFrame jFrame)
    {
        float scale=1f;
        
        if (image.getWidth()>jFrame.getWidth() || image.getHeight()>jFrame.getHeight())
        {
            if (image.getWidth()>jFrame.getWidth() && image.getHeight()<jFrame.getHeight())
                scale = (float)(jFrame.getWidth())/(float)((image.getWidth()+2)); 
            else if (image.getHeight()>jFrame.getHeight() && image.getWidth()<jFrame.getWidth())
                scale = (float)(jFrame.getHeight())/(float)((image.getHeight()+2));  
            else 
            {
                float s1 = (float)(jFrame.getWidth())/(float)((image.getWidth()+2)); 
                float s2 = (float)(jFrame.getHeight())/(float)((image.getHeight()+2));
                if (s1>s2) scale = s2; 
                else scale = s1;
            }
        }
        return scale;
    }

    void wstawWyniki(int strona)
    {
        wyczyscOkna();
        int start = (strona-1)*4;
        int stop = 4*strona-1;
        int m = 3;
        for (int i=start; i<=stop && i<list.size(); i++)
        {
            JLabel jta = gettextArea(m-2);
            JLabel jta2 = gettextArea2(m-2);
            //jta.setText(list.get(i).opis);
            jta.setText(list.get(i).model.name);
            if (list.get(i).dopasowaie!=200f)
                jta2.setText(String.valueOf((list.get(i).dopasowaie))+"%");
            //System.out.println(i);
            ustawObrazek(list.get(i), m);
            m = m+1;
        }
    }
    
    JLabel gettextArea(int i)
    {
        if (i==1)
            return jLabel3;
        else if (i==2)
            return jLabel2;
        else if (i==3)
            return jLabel4;
        else 
            return jLabel5;
    }
    
    JLabel gettextArea2(int i)
    {
        if (i==1)
            return jLabel6;
        else if (i==2)
            return jLabel7;
        else if (i==3)
            return jLabel8;
        else 
            return jLabel9;
    }
    
    void wyczyscOkna()
    {
        String comName;
        jLabel2.setText("");
        jLabel3.setText("");
        jLabel4.setText("");
        jLabel5.setText("");
        jLabel6.setText("");
        jLabel7.setText("");
        jLabel8.setText("");
        jLabel9.setText("");
        for (int i = 3; i<7; i++)
        {
            comName="f"+i;
        
            JInternalFrame jif = (JInternalFrame) getComponentByName(comName);
        
            try {
                jif.setVisible(false);
                jif.setSelected(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Container parent = jif.getContentPane().getParent();
            if (parent!=null)
            {
                parent.remove(jif.getContentPane());
                parent.validate();
                parent.repaint();
            }
            

            //contentPane.removeAll();
            //contentPane.add(panel);
            //contentPane.revalidate(); 
            //contentPane.repaint();
        
            try {
                jif.setVisible(true);
                jif.setSelected(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
        
    }
    
    void ustawObrazek(Result r, int i)
    {
        String comName = "f"+i;
        //System.out.println("comName "+comName);
        
        JInternalFrame jif = (JInternalFrame) getComponentByName(comName);
        
        //System.out.println("jif "+componentMap.size());
        
        try {
            jif.setVisible(false);
            jif.setSelected(false);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        BufferedImage image = null;
        try
        {
          System.out.println(" r model file ame "+r.model.file_name);
          image = ImageIO.read(new File(r.model.file_name));
        }
        catch (Exception e)
        {
          e.printStackTrace();
          System.exit(1);
        }
        
        float scale=getScale(image, jif);
        
        ImageIcon imageIcon = new ImageIcon(image.getScaledInstance((int)(image.getWidth()*scale), (int)(image.getHeight()*scale), 0));
        JLabel jLabel = new JLabel(imageIcon, SwingConstants.CENTER);
        jif.setContentPane(jLabel);
        try {
            jif.setVisible(true);
            jif.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        } 
    
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jInternalFrame3 = new javax.swing.JInternalFrame();
        jInternalFrame4 = new javax.swing.JInternalFrame();
        jInternalFrame5 = new javax.swing.JInternalFrame();
        jInternalFrame6 = new javax.swing.JInternalFrame();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        jButton3 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(883, 621));
        setMinimumSize(new java.awt.Dimension(883, 621));
        setResizable(false);

        jInternalFrame3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jInternalFrame3.setAutoscrolls(true);
        jInternalFrame3.setFrameIcon(null);
        jInternalFrame3.setMaximumSize(new java.awt.Dimension(459, 384));
        jInternalFrame3.setMinimumSize(new java.awt.Dimension(459, 384));
        jInternalFrame3.setName(""); // NOI18N
        jInternalFrame3.setPreferredSize(new java.awt.Dimension(459, 384));
        jInternalFrame3.setVerifyInputWhenFocusTarget(false);
        jInternalFrame3.setVisible(true);
        jInternalFrame3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jInternalFrame3MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jInternalFrame3Layout = new javax.swing.GroupLayout(jInternalFrame3.getContentPane());
        jInternalFrame3.getContentPane().setLayout(jInternalFrame3Layout);
        jInternalFrame3Layout.setHorizontalGroup(
            jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrame3Layout.setVerticalGroup(
            jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jInternalFrame4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jInternalFrame4.setAutoscrolls(true);
        jInternalFrame4.setFrameIcon(null);
        jInternalFrame4.setMaximumSize(new java.awt.Dimension(459, 384));
        jInternalFrame4.setMinimumSize(new java.awt.Dimension(459, 384));
        jInternalFrame4.setName(""); // NOI18N
        jInternalFrame4.setPreferredSize(new java.awt.Dimension(459, 384));
        jInternalFrame4.setVerifyInputWhenFocusTarget(false);
        jInternalFrame4.setVisible(true);
        jInternalFrame4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jInternalFrame4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jInternalFrame4Layout = new javax.swing.GroupLayout(jInternalFrame4.getContentPane());
        jInternalFrame4.getContentPane().setLayout(jInternalFrame4Layout);
        jInternalFrame4Layout.setHorizontalGroup(
            jInternalFrame4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrame4Layout.setVerticalGroup(
            jInternalFrame4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jInternalFrame5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jInternalFrame5.setAutoscrolls(true);
        jInternalFrame5.setFrameIcon(null);
        jInternalFrame5.setMaximumSize(new java.awt.Dimension(459, 384));
        jInternalFrame5.setMinimumSize(new java.awt.Dimension(459, 384));
        jInternalFrame5.setName(""); // NOI18N
        jInternalFrame5.setPreferredSize(new java.awt.Dimension(459, 384));
        jInternalFrame5.setVerifyInputWhenFocusTarget(false);
        jInternalFrame5.setVisible(true);
        jInternalFrame5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jInternalFrame5MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jInternalFrame5Layout = new javax.swing.GroupLayout(jInternalFrame5.getContentPane());
        jInternalFrame5.getContentPane().setLayout(jInternalFrame5Layout);
        jInternalFrame5Layout.setHorizontalGroup(
            jInternalFrame5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrame5Layout.setVerticalGroup(
            jInternalFrame5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jInternalFrame6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jInternalFrame6.setAutoscrolls(true);
        jInternalFrame6.setFrameIcon(null);
        jInternalFrame6.setMaximumSize(new java.awt.Dimension(459, 384));
        jInternalFrame6.setMinimumSize(new java.awt.Dimension(459, 384));
        jInternalFrame6.setName(""); // NOI18N
        jInternalFrame6.setPreferredSize(new java.awt.Dimension(459, 384));
        jInternalFrame6.setVerifyInputWhenFocusTarget(false);
        jInternalFrame6.setVisible(true);
        jInternalFrame6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jInternalFrame6MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jInternalFrame6Layout = new javax.swing.GroupLayout(jInternalFrame6.getContentPane());
        jInternalFrame6.getContentPane().setLayout(jInternalFrame6Layout);
        jInternalFrame6Layout.setHorizontalGroup(
            jInternalFrame6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrame6Layout.setVerticalGroup(
            jInternalFrame6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jButton1.setText("Wstecz");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        jButton2.setText("Dalej");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("jLabel2");
        jLabel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("jLabel2");
        jLabel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("jLabel2");
        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("jLabel2");
        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("jLabel6");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("jLabel6");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("jLabel6");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("jLabel6");

        buttonGroup1.add(jCheckBox1);
        jCheckBox1.setName("b1"); // NOI18N

        buttonGroup1.add(jCheckBox2);
        jCheckBox2.setName("b2"); // NOI18N

        buttonGroup1.add(jCheckBox3);
        jCheckBox3.setName("b3"); // NOI18N

        buttonGroup1.add(jCheckBox4);
        jCheckBox4.setName("b4"); // NOI18N

        jButton3.setText("Zaakceptuj wybrany model");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Wyniki rozpoznania");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jInternalFrame5, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBox3))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jInternalFrame3, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jCheckBox1))))
                        .addGap(0, 12, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jInternalFrame6, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBox4))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jInternalFrame4, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jCheckBox2))))
                        .addContainerGap(24, Short.MAX_VALUE))))
            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jInternalFrame3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jInternalFrame4, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jInternalFrame5, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jInternalFrame6, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox3)
                    .addComponent(jCheckBox4))
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton3)))
                .addGap(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        
        if (strona==1)
        {}
        else 
        {
            strona = strona-1;
            jLabel1.setText(strona+"/"+liczbaStron);
            wstawWyniki(strona);
        }
    }//GEN-LAST:event_jButton1MouseClicked

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
       
        if (strona==liczbaStron)
        {}
        else 
        {
            strona = strona+1;
            jLabel1.setText(strona+"/"+liczbaStron);
            wstawWyniki(strona);
        }
    }//GEN-LAST:event_jButton2MouseClicked

    private void jInternalFrame3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jInternalFrame3MouseClicked
        
        //
        int index = strona*4-4;
        if (index<list.size())
        {
            GlassDetailsView frame = new GlassDetailsView(list.get(index).model);
            frame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
        }
        
    }//GEN-LAST:event_jInternalFrame3MouseClicked

    private void jInternalFrame4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jInternalFrame4MouseClicked
        
        int index = strona*4-3;
        if (index<list.size())
        {
            GlassDetailsView frame = new GlassDetailsView(list.get(index).model);
            frame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
        }
    }//GEN-LAST:event_jInternalFrame4MouseClicked

    private void jInternalFrame5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jInternalFrame5MouseClicked
        
        int index = strona*4-2;
        if (index<list.size())
        {
            GlassDetailsView frame = new GlassDetailsView(list.get(index).model);
            frame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
        }
    }//GEN-LAST:event_jInternalFrame5MouseClicked

    private void jInternalFrame6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jInternalFrame6MouseClicked
       
        int index = strona*4-1;
        if (index<list.size())
        {
            GlassDetailsView frame = new GlassDetailsView(list.get(index).model);
            frame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
        }
    }//GEN-LAST:event_jInternalFrame6MouseClicked

    public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getName();
            }
        }
        return "";
    }
    
    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        
        String boxName = getSelectedButtonText(buttonGroup1);
        int index;
            if (boxName.equals("b1"))
                index = strona*4-4;
            else if (boxName.equals("b2"))
                index = strona*4-3;
            else if (boxName.equals("b3"))
                index = strona*4-2;
            else
                index = strona*4-1;
            
        if (index<list.size())
        {
           int id1 = Connection.getInstance().getLastModelId();
           int id2 = Connection.getInstance().getLastInstanceId();
           String path = "res/kodano/"+id1+"_"+id2+".jpg";
            try {
                ImageIO.write(ImageIO.read(new File(NewJFrame.filePath)),"jpg", new File(path));
            } catch (IOException ex) {
                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }    
           Model m = list.get(index).model;
           Connection.getInstance().insertNewInstance(path, m);
           JOptionPane.showMessageDialog(new NewJFrame(), "Dodano do bazy");
        }
     
    }//GEN-LAST:event_jButton3MouseClicked

    void setText(String s)
    {
        jLabel1.setText(s);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JInternalFrame jInternalFrame3;
    private javax.swing.JInternalFrame jInternalFrame4;
    private javax.swing.JInternalFrame jInternalFrame5;
    private javax.swing.JInternalFrame jInternalFrame6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    // End of variables declaration//GEN-END:variables
}

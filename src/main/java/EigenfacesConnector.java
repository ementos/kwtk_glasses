package main;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.*;
import org.opencv.highgui.*;
import java.util.*;
import org.opencv.utils.Converters;

public class EigenfacesConnector {
    static {
        System.loadLibrary("eigenfaces");
    }

    public native int[] svmPredict(String path, String[] trainingSetPaths);
    public native void svmNormalize(String binaryPath, String imagePath, String outputPath);

    String[] Predict(String path) {
        LLogger.getLogger().info("Run eigenfaces prediction");
        String[] filesSet = Connection.getInstance().getTrainingModels();
        int[] predictedLabels = this.svmPredict(path, filesSet);
        String[] predictedFiles = new String[predictedLabels.length];
        int i = 0;
        for (int label : predictedLabels) {
            predictedFiles[i++] = filesSet[label];
        }

        LLogger.getLogger().info("Predicted "+predictedLabels.length +" models");

        return predictedFiles;
    }

    void Normalize(String binaryPath, String imagePath, String outputPath) {
        this.svmNormalize(binaryPath, imagePath, outputPath);
    }

    String Normalize(Mat binaryImage, Mat originalImage) {
        LLogger.getLogger().info("Run eigenfaces normalization");
        Highgui.imwrite("binaryImageForNormalization.jpg", binaryImage);
        Highgui.imwrite("imageForNormalization.jpg", originalImage);
        this.Normalize("binaryImageForNormalization.jpg", "imageForNormalization.jpg", "outNormalizedImage.jpg");

        return "outNormalizedImage.jpg";
    }
}

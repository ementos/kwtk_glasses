package main;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import static main.GlassTemplesRemover.thresVal;

public class GlassesBrowser extends javax.swing.JFrame {
    JPanel galleryPanel;
    String filePath;
    Connection connection;

    public GlassesBrowser() {
        connection = Connection.getInstance();
        setMinimumSize(new java.awt.Dimension(840, 480));
        initComponents();
    }

    @SuppressWarnings("unchecked")

    float getScale(BufferedImage image, JInternalFrame jFrame)
    {
        float scale=1f;

        if (image.getWidth()>jFrame.getWidth() || image.getHeight()>jFrame.getHeight())
        {
            if (image.getWidth()>jFrame.getWidth() && image.getHeight()<jFrame.getHeight())
                scale = (float)(jFrame.getWidth())/(float)((image.getWidth()+2));
            else if (image.getHeight()>jFrame.getHeight() && image.getWidth()<jFrame.getWidth())
                scale = (float)(jFrame.getHeight())/(float)((image.getHeight()+2));
            else
            {
                float s1 = (float)(jFrame.getWidth())/(float)((image.getWidth()+2));
                float s2 = (float)(jFrame.getHeight())/(float)((image.getHeight()+2));
                if (s1>s2) scale = s2;
                else scale = s1;
            }
        }
        return scale;
    }

    private void initComponents() {
        final JPanel windowPanel = new JPanel();
        windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.Y_AXIS));

        galleryPanel = new JPanel();
        GridLayout galleryLayout = new GridLayout(0, 3, 2, 2);
        galleryPanel.setLayout(galleryLayout);
//        galleryPanel.setPreferredSize(new Dimension(620, 480));
        galleryPanel.setAutoscrolls(true);
        javax.swing.JLabel titleLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Identyfikacja modeli okularów");
        setLocationRelativeTo(null);

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        titleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleLabel.setText("Przeglądanie bazy modeli okularów");
        titleLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        windowPanel.add(titleLabel);

        JTextField seachEntry = new JTextField();
        seachEntry.setMaximumSize(new Dimension(Integer.MAX_VALUE, 20));
        seachEntry.addKeyListener(new KeyListener() {
            String lastText;
            @Override
            public void keyTyped(KeyEvent event) {
            }

            @Override
            public void keyReleased(KeyEvent event) {
                String text = seachEntry.getText();
                if (text.length() > 2) {
                    fillGallery(text);
                    galleryPanel.revalidate();
                } else if (lastText != null && lastText.length() < 4) {
                    fillGallery(null);
                    galleryPanel.revalidate();
                }

                lastText = text;
            }

            @Override
            public void keyPressed(KeyEvent event) {
            }
        });

        windowPanel.add(seachEntry);

        final JScrollPane galleryScrollbar = new JScrollPane(galleryPanel);
        add(galleryScrollbar, BorderLayout.PAGE_START);

        windowPanel.add(galleryScrollbar);
        add(windowPanel);

        fillGallery(null);
    }

    private void fillGallery(String content) {
        galleryPanel.removeAll();
        ArrayList<Model> allModels = connection.getAllModels(content);
        for (Model model : allModels) {
            galleryPanel.add(new GlassesBrowserComponent(model));
        }
    }
}

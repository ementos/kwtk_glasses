package main;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.*;
import org.opencv.highgui.*;
import java.util.*;

public class GlassNormalizer {
    class GlassesPoints {
        Point glass1;
        Point glass2;
    }

    Size size;
    MatOfPoint mainContour;

    GlassNormalizer(Size size)
    {
        this.size = size;
    }

    Mat resultImg;

    public Mat Normalize(MatOfPoint mainContour, Mat grayImage)
    {
        Mat returnImage = grayImage.clone();
        this.resultImg = returnImage;

        if (mainContour == null)
        {
            System.out.println("Couldn't find contur");
            return returnImage;
        }

        RotatedRect rect = Imgproc.minAreaRect(new MatOfPoint2f(mainContour.toArray()));

        Point[] vertices = new Point[4];
        rect.points(vertices);
        for(int i=0; i<4; ++i)
        {
            Core.line(returnImage, vertices[i], vertices[(i+1)%4], new Scalar(100,100,100));
        }

        System.out.println("rect: " + rect.angle);

        Size rotatedSize = rect.size;
        Mat rotationMatrix;
        if (Between(Math.abs(rect.angle), 0.0, 45.0)) {
            rotationMatrix = Imgproc.getRotationMatrix2D(rect.center, rect.angle, 1.0);
        } else {
            rotationMatrix = Imgproc.getRotationMatrix2D(rect.center, rect.angle + 90, 1.0);
            rotatedSize = new Size(rect.size.height, rect.size.width);
        }

        Imgproc.warpAffine(returnImage, returnImage, rotationMatrix, returnImage.size());

        Imgproc.getRectSubPix(returnImage, rotatedSize, rect.center, returnImage);

        Imgproc.resize(returnImage, returnImage, size);

        return returnImage;
    }

    MatOfPoint GetContourAssociatedWithGlasses(Mat grayImage, GlassesPoints points)
    {
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat binaryImage = new Mat();
        Imgproc.Canny(grayImage, binaryImage, 10, 100, 3, true);
        Imgproc.findContours(binaryImage, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        for(int i=0; i < contours.size();i++) {
            MatOfInt hull = new MatOfInt();
            MatOfInt4 defects = new MatOfInt4();
            Imgproc.convexHull(contours.get(i), hull);

            MatOfPoint defectsPoint = defectsHull(hull.toArray(), contours.get(i));
            if (inContour(points.glass1, defectsPoint) &&
                inContour(points.glass2, defectsPoint))
            {
                return contours.get(i);
            }
        }

        return null;
    }

    MatOfPoint defectsHull(int[] points, MatOfPoint contour)
    {
        List<Point> outPoints = new ArrayList<Point>();
        Point[] contourPoints = contour.toArray();
        for(int point : points)
        {
            outPoints.add(contourPoints[point]);
        }
 

        MatOfPoint pointsVector = new MatOfPoint();
        pointsVector.fromList(outPoints);

        return pointsVector;
    }

    boolean inContour(Point p, MatOfPoint c)
    {
        MatOfPoint2f m = new MatOfPoint2f(c.toArray());

        double r = Imgproc.pointPolygonTest(m, p, false);

        return r >= 0;
    }

    private GlassesPoints FindGlasses(Mat grayImage)
    {
        GlassesPoints points = null;

        Mat circles = new Mat();
        Imgproc.HoughCircles(grayImage, circles, Imgproc.CV_HOUGH_GRADIENT, 4.0, 100.0);

        for (int i = 0; i < circles.cols() - 1; i++)
        {
            double[] vecCircle = circles.get(0, i);
            Point p = new Point(vecCircle[0], vecCircle[1]);
            int r = (int) vecCircle[2];

            for (int j = i + 1; j < circles.cols(); j++)
            {
                double[] vecCircle2 = circles.get(0, j);
                int r2 = (int) vecCircle2[2];
                int radiusDiff = Math.abs(r - r2);
                double relativeRadiusDiff = 2*radiusDiff / (double)(r + r2);
                System.out.println("Radiuses " + r + ","+ r2);
                System.out.println("RelativeRadiusDiff " + relativeRadiusDiff);
                System.out.println("RadiusDiff " + radiusDiff);
                if (!Between(relativeRadiusDiff, 0.0, 0.6))
                    continue;
                
                Point p2 = new Point(vecCircle2[0], vecCircle2[1]);
                Point diff = new Point(p.x - p2.x, p.y - p2.y);
                double distance = Math.sqrt(diff.x*diff.x + diff.y + diff.y);
                double relativeDistance = 2*distance / (r + r2);
                if (Between(relativeDistance, 1.8, 3)) {
                    Core.circle(grayImage, p, r, new Scalar(5, 5, 25), 3);
                    Core.circle(grayImage, p2, r2, new Scalar(5, 5, 25), 3);

                    points = new GlassesPoints();
                    points.glass1 = p;
                    points.glass2 = p2;

                    break;
                }
            }
        }

        return points;
    }

    private static boolean Between(double value, double threshold1, double threshold2)
    {
        return value > threshold1 && value < threshold2;
    }
}

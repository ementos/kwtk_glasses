package main;

import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ola
 */
public class Connection {

// public static FileWriter zapis2 ;

    public static java.sql.Connection conn = null;
    public static Connection instance = null;

    public static Connection getInstance() {
        if (instance == null) {
            instance = new Connection();
            instance.connect();
        }

        return instance;
    }

    public static void connect()
    {
        try
        {
            String userName = "root";
            String password = "ludwik";
            String sterownik = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/kwtk";
            Class.forName (sterownik);
            conn = DriverManager.getConnection (url, userName, password);
            System.out.println ("Database connection established");
        }
        catch (Exception e)
        {
            System.out.println(e.getClass());
        }
    }
    
    public void insertDescriptors(String nazwaModelu, String nazwaZdjecia)  //to do testowania
    {
        double[] descriptors = EllipticFourierDescriptor.efd;
        String insertModel = "INSERT INTO Model (nazwa) VALUES ('"+nazwaModelu+"');";
        String model = "wzorcowy";
        //String path = NewJFrame.filePath;
       // String[] parts2 = path.split("\\");
       // String nazwaInstancji = parts2[parts2.length-1];
        try{           
            Statement stmt2 = conn.createStatement();
            stmt2.execute(insertModel,Statement.RETURN_GENERATED_KEYS);
            ResultSet rss =stmt2.getGeneratedKeys();
            int modelId =-1;
            if (rss.next()){
                modelId=rss.getInt(1);
            }
            System.out.println(modelId);
            String insertInstance = "INSERT INTO InstancjaModelu (nazwaZdjecia, zdjecie, idModelu) VALUES ('"+model+"','"+nazwaZdjecia+"',"+modelId+");";
            Statement stmt4 = conn.createStatement();
            stmt4.execute(insertInstance,Statement.RETURN_GENERATED_KEYS);
            ResultSet rs =stmt4.getGeneratedKeys();
            int instanceId =-1;
            if (rs.next()){
                instanceId=rs.getInt(1);
            }
            
            //stmt.close();           
            
            String agreguj = "INSERT INTO Deskryptor (wartoscDeskryptora,idInstancjiModelu) VALUES ('"+descriptors[0]+"','"+instanceId+"')";
            for (int i=1;i<descriptors.length;i++)
            {
                agreguj += ",('"+descriptors[i]+"','"+instanceId+"')";

            }
            agreguj +=";";
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(agreguj);
        }
        catch(SQLException ex)
        {
            System.err.println("Insert nieudany");
            LLogger.getLogger().warning("Couldn't insert into Deskryptor");
        }

    }
    
    public void insertNewInstance(String zdjecieSciezka, Model m)
    {

        try{
            double[] descriptors = EllipticFourierDescriptor.efd;
            String insertInstance = "INSERT INTO InstancjaModelu (nazwaZdjecia, zdjecie, idModelu) VALUES ('wzorzec fabryczny','"+zdjecieSciezka+"',"+m.id+");";
            Statement stmt4 = conn.createStatement();
            stmt4.execute(insertInstance,Statement.RETURN_GENERATED_KEYS);
            ResultSet rs =stmt4.getGeneratedKeys();
            int instanceId =-1;
            if (rs.next()){
                instanceId=rs.getInt(1);
            }
            
            
            String agreguj = "INSERT INTO Deskryptor (wartoscDeskryptora,idInstancjiModelu) VALUES ('"+descriptors[0]+"','"+instanceId+"')";
            for (int i=1;i<descriptors.length;i++)
            {
                agreguj += ",('"+descriptors[i]+"','"+instanceId+"')";

            }
            agreguj +=";";
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(agreguj);
        }
        catch(SQLException ex)
        {
            LLogger.getLogger().warning("Couldn't insert new instance");
        }

    }
    public int[] selectInstancesID()
    {
        int[] id;
        try{
            int k;
            String pol2 = "SELECT COUNT(*) FROM InstancjaModelu;";
            Statement stmt2 = conn.createStatement();
            ResultSet rs2 = stmt2.executeQuery(pol2);
            if(rs2.next()){ //Retrieve by column name int id = rs.getInt("id");
                k = rs2.getInt(1);
                id = new int[k];
                String pol = "SELECT idInstancjiModelu FROM InstancjaModelu;";
                Statement stmt = conn.createStatement();
                ResultSet rs1 = stmt.executeQuery(pol);
                int i=0;
                while(rs1.next()){ //Retrieve by column name int id = rs.getInt("id");
                    id[i] = rs1.getInt(1);
                    i++;
                }

            }
            else
                id=new int[0];
            return id;
        }
        catch(SQLException ex)
        {
            LLogger.getLogger().warning("Couldn't get model instances");
            return id=new int[0];
        }
    }

    public List<Double> selectDescriptors(int instanceID)
    {
        List<Double> descriptors = new ArrayList<>();
        try{
            String pol = "SELECT idDeskryptora,wartoscDeskryptora FROM Deskryptor WHERE "
                    + "idInstancjiModelu= '"+instanceID+"' ORDER BY idDeskryptora ASC;";
            Statement stmt2 = conn.createStatement();
            ResultSet rs2 = stmt2.executeQuery(pol);
            while(rs2.next()){ //Retrieve by column name int id = rs.getInt("id");
                descriptors.add(rs2.getDouble(2));
            }
        }
        catch(SQLException ex)
        {
            LLogger.getLogger().warning("Couldn't get descriptor for " + instanceID);
        }
        return descriptors;
    }

    public Model selectModelID(int instanceID)
    {
        Model m;
        try{
            String pol = "SELECT m.idModelu,nazwa,zdjecie FROM Model m JOIN InstancjaModelu i ON m.idModelu=i.idModelu "
                    + "WHERE idInstancjiModelu='"+instanceID+"';";
            Statement stmt2 = conn.createStatement();
            ResultSet rs = stmt2.executeQuery(pol);
            m = new Model(0,"","");
            if(rs.next()){ 
                int modelID = rs.getInt(1);
                String name = rs.getString(2);
                String file_name = rs.getString(3);
                m = new Model(modelID,name,file_name);
            }
            //m = new Model(0,"","");
        }
        catch(SQLException ex)
        {
            m = new Model(0,"","");
            LLogger.getLogger().warning("Couldn't get model instance for " + instanceID);
        }
        return m;

    }
    
        public Model getModelInfo(String zdjecieSciezka)
    {
        Model m;
        try{

            String pol = "SELECT i.idModelu,nazwa FROM Model m JOIN InstancjaModelu i ON m.idModelu=i.idModelu "
                    + "WHERE zdjecie='"+zdjecieSciezka+"';";
            Statement stmt2 = conn.createStatement();
            ResultSet rs = stmt2.executeQuery(pol);
            m = new Model(0,"","");
            if(rs.next()){ 
                int modelID = rs.getInt(1);
                String name = rs.getString(2);
                String file_name = zdjecieSciezka;
                m = new Model(modelID,name,file_name);
            }
            //m = new Model(0,"","");
        }
        catch(SQLException ex)
        {
            m = new Model(0,"","");
            System.err.println("Nie udało się pobrać z bazy informacji o instancji modelu");
        }
        return m;

    }
    
    public int getLastModelId(){
        int modelID=-1;
        try{           
        String getLastId = "SELECT idModelu FROM Model ORDER BY idModelu DESC LIMIT 1;";
        Statement stmt2 = conn.createStatement();
            ResultSet rs = stmt2.executeQuery(getLastId);
            if(rs.next()){ 
                modelID = rs.getInt(1)+1;
            }else{
                modelID = 1;
            }
        }
        catch (SQLException ex){
            System.err.println("Nie udało się pobrać z bazy ostatniego id modelu");
        }
        return modelID;
    }
    
    public int getLastInstanceId(){
        String getLastId = "SELECT idInstancjiModelu FROM InstancjaModelu ORDER BY idInstancjiModelu DESC LIMIT 1;";
        int instanceID=-1;
        try{           
        Statement stmt2 = conn.createStatement();
            ResultSet rs = stmt2.executeQuery(getLastId);
            if(rs.next()){ 
                instanceID = rs.getInt(1)+1;
            }else{
                instanceID = 1;
            }
        }
        catch (SQLException ex){
            System.err.println("Nie udało się pobrać z bazy ostatniego id modelu");
        }
        return instanceID;
    }
    

    public ArrayList<Model> getAllModels(String modelName)
    {
        ArrayList<Model> models = new ArrayList<>();
        try{

            String pol;
            if (modelName == null)
                pol = "SELECT m.idModelu,nazwa,zdjecie FROM Model m join InstancjaModelu im on m.idModelu = im.idModelu where nazwaZdjecia='training';";
            else
                pol = "SELECT m.idModelu,nazwa,zdjecie FROM Model m join InstancjaModelu im on m.idModelu = im.idModelu where nazwa like '%"+modelName+"%' and nazwaZdjecia='training';";

            Statement stmt2 = conn.createStatement();
            ResultSet rs = stmt2.executeQuery(pol);
            while(rs.next()) {
                int modelID = rs.getInt(1);
                String name = rs.getString(2);
                String file_name = rs.getString(3);
                Model m = new Model(modelID,name,file_name);
                models.add(m);
            }
        }
        catch(SQLException ex)
        {
            System.err.println("Nie udało się pobrać z bazy informacji o instancjach modelu");
            System.out.print(ex);
        }

        return models;
    }

    public String[] getTrainingModels()
    {
        ArrayList<String> models = new ArrayList<>();
        try{
            String pol = "SELECT zdjecie FROM InstancjaModelu;";

            Statement stmt2 = conn.createStatement();
            ResultSet rs = stmt2.executeQuery(pol);
            while(rs.next()) {
                models.add(rs.getString(1));
            }
        }
        catch(SQLException ex)
        {
            System.err.println("Nie udało się pobrać z bazy informacji o instancjach modelu");
            System.out.print(ex);
        }

        String[] returnArray = new String[models.size()];
        returnArray = models.toArray(returnArray);

        return returnArray;
    }

    public ArrayList<ModelInstance> getAllModelInstances(int modelId)
    {
        ArrayList<ModelInstance> models = new ArrayList<>();
        try{

            String pol = "SELECT im.idInstancjiModelu,nazwaZdjecia,zdjecie FROM InstancjaModelu im where im.idModelu = '"+modelId+"';";

            Statement stmt2 = conn.createStatement();
            ResultSet rs = stmt2.executeQuery(pol);
            while(rs.next()) {
                int instanceID = rs.getInt(1);
                String name = rs.getString(2);
                String file_name = rs.getString(3);
                ModelInstance m = new ModelInstance(modelId, instanceID, name,file_name);
                models.add(m);
            }
        }
        catch(SQLException ex)
        {
            System.err.println("Nie udało się pobrać z bazy informacji o instancjach modelu");
            System.out.print(ex);
        }

        return models;
    }
}


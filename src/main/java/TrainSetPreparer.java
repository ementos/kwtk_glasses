package main;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.*;
import org.opencv.highgui.*;
import java.util.*;

public class TrainSetPreparer {
    TrainSetPreparer()
    {
    }

    void Prepare(String directoryName)
    {
        for (int i = 1; i < 10; i++) {
            String fileName = "image" + i + ".jpg";
            String filePath = directoryName + fileName;
            Mat img;
            try {
                // Load image
                img = Highgui.imread(filePath, Highgui.CV_LOAD_IMAGE_COLOR);
                if (img == null && img.rows() < 1) {
                    continue;
                }
            }
            catch (Exception e)
            {
                continue;
            }

            Mat grayImage = new Mat();
            Imgproc.cvtColor(img, grayImage, Imgproc.COLOR_BGR2GRAY);

            MainContourDetection mcDetector = new MainContourDetection();
            MatOfPoint mainContour = mcDetector.Detect(grayImage);

            GlassNormalizer normalizer = new GlassNormalizer(new Size(250,150));
            Mat normalizedImage = normalizer.Normalize(mainContour, grayImage);

            Highgui.imwrite("cpp_project/res/" + fileName, normalizedImage);
        }
    }
}

package main;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ola
 */
public class EllipticFourierDescriptor {
    
 private double[] x; //the x and y coordinates
  private double[] y;
  /* The number of points on input contour */
  private int m;
  /* The number of FD coefficients */
  public int nFD; 
  /* The Fourier Descriptors */
  public double[] ax, ay, bx, by;
  /* The normalized Elliptic Fourier Descriptors */
  public static double[] efd;
  
  
  /*
  x,y to wspolrzedne punktow konturu
  nFD to liczba deskryptorow które będą obliczone
  */
  public EllipticFourierDescriptor(double[] x, double[] y, int n){
      this.x = x;
      this.y = y;
      this.nFD = n;
      this.m = x.length;
      computeEllipticFD(); 
  }
  
  public EllipticFourierDescriptor(){
  }
 
  /*
  x,y to wspolrzedne punktow konturu
  nFD to liczba deskryptorow które będą obliczone (tu domyślnie połowa liczby punktów)
  */
  public EllipticFourierDescriptor(double[] x, double[] y){ 
      this.x = x;
      this.y = y;
      this.nFD = x.length/2;
      this.m = x.length;
      computeEllipticFD();       
  }
  
  
  /**
    * Computes the Fourier and Elliptic Fourier Descriptors
    */
  private void computeEllipticFD(){
   
    // the fourier descriptors
    ax = new double[nFD];
    ay = new double[nFD];
    bx = new double[nFD];
    by = new double[nFD];
     
    
    double t = 2.0*Math.PI/m;
    double p = 0.0;
    double twoOverM = 2.0/m;
    //step for each FD
    for (int k = 0; k < nFD; k++){
      //for each point
      for (int i = 0; i < m; i++){
        p = k*t*i;
        ax[k] +=  x[i]*Math.cos(p);
        bx[k] +=  x[i]*Math.sin(p);
        ay[k] +=  y[i]*Math.cos(p);
        by[k] +=  y[i]*Math.sin(p);
      }
      
      
      ax[k] *= twoOverM;
      bx[k] *= twoOverM;
      ay[k] *= twoOverM;
      by[k] *= twoOverM;
      
  /*    System.out.println("ax"+k+" "+ ax[k]);
      System.out.println("ay"+k+" "+ ay[k]);
      System.out.println("bx"+k+" "+ bx[k]);
      System.out.println("by"+k+" "+ by[k]);  */
      
      

      
    }
    
    
    //now compute the elliptic fourier descriptors
    efd = new double[nFD];
    int first = 1; //index of the normalization values
    //precompute the denominators
    double denomA = (ax[first]*ax[first]) + (ay[first]*ay[first]);
    double denomB = (bx[first]*bx[first]) + (by[first]*by[first]);
    for (int k = 0; k < nFD; k++){
      efd[k] = Math.sqrt((ax[k]*ax[k] + ay[k]*ay[k])/denomA) + 
        Math.sqrt((bx[k]*bx[k] + by[k]*by[k])/denomB);

      //System.out.println("efd"+k+" "+ efd[k]);
    }
      
          
  }
  

  /*przeksztalcenie odwrotne z deskryptorow do par punktow (x,y)
  zwraca tablicę punktów ktora jest takiej samej dlugosci jak wejsciowy zbior punktow konturu
  (czyli ten przed przekształcenie do Fouriera) */
  public double[][] createPolygon(){
    double p = 0.0;
    double[][] xy = new double[m][2];
    double t = 2.0*Math.PI/m;
    for (int i = 0; i < m; i++){
     xy[i][0] = ax[0]/2.0;
     xy[i][1] = ay[0]/2.0;
     
     for (int k = 1; k < nFD; k++){
      p = t * k * i;
      xy[i][0] += ax[k]*Math.cos(p) + bx[k] * Math.sin(p);
      xy[i][1] += ay[k]*Math.cos(p) + by[k] * Math.sin(p);
     } 
    }
    return xy;
  }


  //przeksztalcenie odwrotne z deskryptorow do par punktow (x,y)
  public int[][] createPolygonInt(){
    double p = 0.0;
    double[][] xy = new double[m][2];
    int[][] ixy = new int[m][2];
    double t = 2.0*Math.PI/m;
    for (int i = 0; i < m; i++){
     xy[i][0] = ax[0]/2.0;
     xy[i][1] = ay[0]/2.0;
     
     for (int k = 1; k < nFD; k++){
      p = t * k * i;
      xy[i][0] += ax[k]*Math.cos(p) + bx[k] * Math.sin(p);
      xy[i][1] += ay[k]*Math.cos(p) + by[k] * Math.sin(p);
      ixy[i][0] = (int) xy[i][0];
      ixy[i][1] = (int) xy[i][1];
      
     } //k-loop through the FDs
    }//i-loop through the points
    return ixy;
  }
  
  public double[] getEFD(){
      return efd;
  }
  
    public double measureEuclideanDistance(double[] vector1, List<Double> vector2){
        // initialize matrix of uchar of 1-channel where you will store vec data
        Mat m = new Mat(vector1.length, 1,CvType.CV_64F); 
        //copy vector to mat
        for (int l=0;l<vector1.length;l++)
            m.put(l, 0, vector1[l]);
        // initialize matrix of uchar of 1-channel where you will store vec data
        Mat m2 = new Mat(vector2.size(), 1,CvType.CV_64F); 
        //copy vector to mat
        for (int l=0;l<vector2.size();l++)
            m2.put(l, 0, vector2.get(l));

        double dist = Core.norm(m,m2,Core.NORM_L2);
        System.out.println("odległość euklidesowa między wektorami" + dist);
        return dist;
    }
    
    public double measureEuclideanDistance(List<Double> vector1, List<Double> vector2){
        // initialize matrix of uchar of 1-channel where you will store vec data
        Mat m = new Mat(vector1.size(), 1,CvType.CV_64F); 
        //copy vector to mat
        for (int l=0;l<vector1.size();l++)
            m.put(l, 0, vector1.get(l));
        // initialize matrix of uchar of 1-channel where you will store vec data
        Mat m2 = new Mat(vector2.size(), 1,CvType.CV_64F); 
        //copy vector to mat
        for (int l=0;l<vector2.size();l++)
            m2.put(l, 0, vector2.get(l));

        double dist = Core.norm(m,m2,Core.NORM_L2);
        //double similarity = 100/(1+dist)
        System.out.println("odległość euklidesowa między wektorami" + dist);
        return dist;
    }
    
    public List<Result> getListOfMatches(double[] newVector){
        Connection conn = Connection.getInstance();
        conn.connect();
        int[] wzorce = conn.selectInstancesID();
        List<Double>  results = new ArrayList<>();
        List<Result> endResults = new ArrayList<>();
        int descriptorsNumber = newVector.length;
        for (int wzorzec : wzorce){
            List<Double> vectorToCompare = conn.selectDescriptors(wzorzec);
            if (descriptorsNumber==vectorToCompare.size()){
                double distance = measureEuclideanDistance(newVector,vectorToCompare);
                double percent = 100*Math.exp(-Math.pow(distance,2)/(2*25));
                percent = 100/(1+distance);
                System.out.println(percent);
                results.add(percent);
                String.format("%.2f",percent);
                Model m = conn.selectModelID(wzorzec);
                endResults.add(new Result(Float.parseFloat(String.format(Locale.ENGLISH, "%.2f",percent)),m));
            }
        }
        endResults.sort(new Comparator<Result>(){
                public int compare(Result a, Result b){
                    if (a.dopasowaie < b.dopasowaie) return 1;
                    if (a.dopasowaie > b.dopasowaie) return -1;
                    return 0;
                }
        });
        
        List<Result> end = new ArrayList<>(endResults.subList(0, 10));
        
//cała lista wyników posortowanych od najmniejszej odległości Euklidesa (czyli od najbardziej dopasowanych) do największej
        LLogger.getLogger().info("Predicted "+end.size()+" models with Fourier detector");
        return end;    
    }
  
}






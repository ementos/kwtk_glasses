package main;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;

public class GlassesBrowserComponent extends JPanel implements MouseListener {
    Model glass;
    public GlassesBrowserComponent(Model glass) {
        this.glass = glass;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JLabel image_label = new JLabel(new ImageIcon(glass.file_name));
        add(image_label);
        add(new JLabel(glass.name));
        image_label.addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent me){
        GlassDetailsView frame = new GlassDetailsView(glass);
        frame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }

    public void mouseExited(MouseEvent me){
    }

    public void mouseEntered(MouseEvent me){
    }

    public void mouseReleased(MouseEvent me){
    }

    public void mousePressed(MouseEvent me){
    }
}

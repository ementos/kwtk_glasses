package main;

import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.*;
import java.io.*;

public class LLogger {

    public static Logger instance = null;

    public static Logger getLogger() {
        if (instance == null) {
            instance = Logger.getLogger("GlassesLog");
            try {  

                // This block configure the logger with handler and formatter  
                FileHandler fh = new FileHandler("GlassesLog.log", true);  
                instance.addHandler(fh);
                SimpleFormatter formatter = new SimpleFormatter();  
                fh.setFormatter(formatter);  


            } catch (SecurityException e) {  
                e.printStackTrace();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }

        return instance;
    }


}

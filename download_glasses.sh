#!/bin/bash

glassId=1

for pageNumber in {1..45}
do
	siteAddress="https://kodano.pl/okulary-korekcyjne/rodzaj/pe%C5%82ne/page/$pageNumber.html"
	wget $siteAddress -O site

	glasses=`awk 'match($0, /frameFile.*=.*jpg&/) {
		print "https://kodano.pl"substr($0, RSTART+10, RLENGTH-11)
	}
	' site | sort -u`

	for glass in $glasses
	do
		# echo $glass
		wget $glass -P res/kodano/ -O res/kodano/glass_$glassId.jpg
        echo $glass >> GlassesMapping.txt
        let "glassId++"
	done
done

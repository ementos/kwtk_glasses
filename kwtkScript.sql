-- MySQL Script generated by MySQL Workbench
-- 01/10/17 22:52:51
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE DATABASE IF NOT EXISTS `kwtk` DEFAULT CHARACTER SET utf8 ;


-- -----------------------------------------------------
-- Table `mydb`.`Model`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kwtk`.`Model` (
  `idModelu` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nazwa` VARCHAR(40) NULL,
  PRIMARY KEY (`idModelu`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `mydb`.`InstancjaModelu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kwtk`.`InstancjaModelu` (
  `idInstancjiModelu` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `zdjecie` VARCHAR(40) NOT NULL,
  `nazwaZdjecia` VARCHAR(40) NOT NULL,
  `idModelu` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idInstancjiModelu`),
  INDEX `idModelu_idx` (`idModelu` ASC),
  CONSTRAINT `fk_idModelu`
    FOREIGN KEY (`idModelu`)
    REFERENCES `kwtk`.`Model` (`idModelu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Deskryptor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kwtk`.`Deskryptor` (
  `idDeskryptora` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `wartoscDeskryptora` DOUBLE UNSIGNED NOT NULL,
  `idInstancjiModelu` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idDeskryptora`),
  INDEX `fk_idInstancjiModelu_idx` (`idInstancjiModelu` ASC),
  CONSTRAINT `fk_idInstancjiModelu`
    FOREIGN KEY (`idInstancjiModelu`)
    REFERENCES `kwtk`.`InstancjaModelu` (`idInstancjiModelu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Haslo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kwtk`.`Haslo` (
  `idHasla` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(20) NOT NULL,
  `haslo` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`idHasla`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- Vim command to make from GlassesMapping
-- :%s/\(.*\)\. \S\+ \(.*\)$/INSERT INTO `kwtk`.`Model` (idModelu, nazwa) values (\1, "\2");\rINSERT INTO `kwtk`.`InstancjaModelu` (zdjecie, nazwaZdjecia, idModelu) values ("res\/image\1.jpg", "training", \1);/g
-- :%s/\(.*\)\. \S\+ \(.*\)$/\1\2/g
